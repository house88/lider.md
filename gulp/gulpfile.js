var gulp 	= require('gulp');
var concat	= require('gulp-concat');
var minify 	= require('gulp-minify');
var size    = require('gulp-size');

var jsUserSrc 	= [
	'../theme/accent/js/jquery.js',
	'../theme/accent/plugins/moment/moment-with-locales.js',
	'../theme/accent/plugins/moment/config_site.js',
	'../theme/accent/js/bootstrap.min.js',
	'../theme/accent/plugins/tabdrop/js/bootstrap-tabdrop.js',
	'../theme/accent/js/dimmer.js',
	'../theme/accent/js/jquery.cookie.js',
	'../theme/accent/plugins/bxslider/jquery.bxslider.js',
	'../theme/accent/plugins/slick/slick.js',
	'../theme/accent/plugins/fancybox3/jquery.fancybox.js',
	'../theme/accent/plugins/slimScroll/jquery.slimscroll.js',
	'../theme/accent/plugins/icheck/icheck.js',
	'../theme/accent/plugins/tooltipster/tooltipster.bundle.js',
	'../theme/accent/plugins/datatable/js/jquery.dataTables.js',
	'../theme/accent/plugins/datatable/js/dataTables.bootstrap.js',
	'../theme/accent/plugins/datatable/js/jquery.dtFilters.js',
	'../theme/accent/plugins/jsfilters/jquery.filters.js',
	'../theme/accent/plugins/bootstrap-slider/bootstrap-slider.js',
	'../theme/accent/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js',
	'../theme/accent/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.tooltips.js',
	'../theme/accent/plugins/fileupload/jquery.ui.widget.js',
	'../theme/accent/plugins/fileupload/jquery.fileupload.js',
	'../theme/accent/plugins/bootstrap-dialog/bootstrap-dialog.js',
	'../theme/accent/plugins/snow/snowfall.jquery.js',
	'../theme/accent/js/scripts.js',
	'../theme/accent/js/socials_auth.js',
	'../theme/accent/plugins/inputmask/jquery.inputmask.min.js',
	'../theme/accent/plugins/star-rating-svg/main.js',
];

function build(){
	return gulp.src(jsUserSrc).
				pipe(size({showFiles: true})).
				pipe(concat('all-scripts.js')).
				pipe(minify()).
				pipe(gulp.dest('../theme/accent/js/'));
}

exports.build = build;