<!DOCTYPE html>
<html lang="ru">
    <head>
        <title>Сайт временно недоступен</title>
        <meta charset="UTF-8">
        <base href="https://www.atehno.md/">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="theme/accent/maintenance/vendor/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="theme/accent/maintenance/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="theme/accent/maintenance/fonts/iconic/css/material-design-iconic-font.min.css">
        <link rel="stylesheet" type="text/css" href="theme/accent/maintenance/vendor/animate/animate.css">
        <link rel="stylesheet" type="text/css" href="theme/accent/maintenance/css/util.css">
        <link rel="stylesheet" type="text/css" href="theme/accent/maintenance/css/main.css">

        <link rel="apple-touch-icon" sizes="57x57" href="theme/accent/favicon/apple-icon-57x57.png?v=<?php echo uniqid();?>">
        <link rel="apple-touch-icon" sizes="60x60" href="theme/accent/favicon/apple-icon-60x60.png?v=<?php echo uniqid();?>">
        <link rel="apple-touch-icon" sizes="72x72" href="theme/accent/favicon/apple-icon-72x72.png?v=<?php echo uniqid();?>">
        <link rel="apple-touch-icon" sizes="76x76" href="theme/accent/favicon/apple-icon-76x76.png?v=<?php echo uniqid();?>">
        <link rel="apple-touch-icon" sizes="114x114" href="theme/accent/favicon/apple-icon-114x114.png?v=<?php echo uniqid();?>">
        <link rel="apple-touch-icon" sizes="120x120" href="theme/accent/favicon/apple-icon-120x120.png?v=<?php echo uniqid();?>">
        <link rel="apple-touch-icon" sizes="144x144" href="theme/accent/favicon/apple-icon-144x144.png?v=<?php echo uniqid();?>">
        <link rel="apple-touch-icon" sizes="152x152" href="theme/accent/favicon/apple-icon-152x152.png?v=<?php echo uniqid();?>">
        <link rel="apple-touch-icon" sizes="180x180" href="theme/accent/favicon/apple-icon-180x180.png?v=<?php echo uniqid();?>">
        <link rel="icon" type="image/png" sizes="192x192"  href="theme/accent/favicon/android-icon-192x192.png?v=<?php echo uniqid();?>">
        <link rel="icon" type="image/png" sizes="32x32" href="theme/accent/favicon/favicon-32x32.png?v=<?php echo uniqid();?>">
        <link rel="icon" type="image/png" sizes="96x96" href="theme/accent/favicon/favicon-96x96.png?v=<?php echo uniqid();?>">
        <link rel="icon" type="image/png" sizes="16x16" href="theme/accent/favicon/favicon-16x16.png?v=<?php echo uniqid();?>">
        <link rel="manifest" crossorigin="use-credentials" href="theme/accent/favicon/manifest.json?v=<?php echo uniqid();?>">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="theme/accent/favicon/ms-icon-144x144.png?v=<?php echo uniqid();?>">
        <meta name="theme-color" content="#ffffff">
    </head>
    <body>
        <div class="flex-w flex-str size1 overlay1">
            <div class="flex-w flex-col-sb wsize1 bg0 p-l-65 p-t-37 p-b-50 p-r-80 respon1">
                <div class="wrappic1">
                    <a href="#">
                        <img src="theme/accent/css/images/nlogo.png" alt="IMG">
                    </a>
                </div>
                <div class="w-full flex-c-m p-t-80 p-b-90">
                    <div class="wsize2">
                        <h3 class="l1-txt1 p-b-34 respon3">Сайт временно недоступен</h3>
                        <p class="m2-txt1 p-b-46">У нас есть для тебя кое-что особенное.<br>Пожалуйста, возвращайтесь поскорее.</p>

                        <div class="footer-widget footer-widget-contacts">
                            <h4>Контакты</h4>
                            <ul class="list-unstyled">
                                <li>
                                    <i class="fa fa-map-marker"></i>
                                    <a href="https://g.page/atehno-md?gm" target="_blank">Молдова, г. Бельцы, Штефан чел Маре 76а</a>
                                </li>
                                <li><i class="fa fa-phone"></i> <a href="callto:+37323163090">(0231) 6 30 90</a></li>
                                
                                <li><i class="fa fa-envelope"></i> <a href="mailto:info@atehno.md">info@atehno.md</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="wsize1 simpleslide100-parent respon2">
                <div class="simpleslide100">
                    <div class="simpleslide100-item bg-img1" style="background-image: url('theme/accent/maintenance/images/bg01.jpg?v=<?php echo uniqid();?>');"></div>
                    <div class="simpleslide100-item bg-img1" style="background-image: url('theme/accent/maintenance/images/bg02.jpg?v=<?php echo uniqid();?>');"></div>
                    <div class="simpleslide100-item bg-img1" style="background-image: url('theme/accent/maintenance/images/bg03.jpg?v=<?php echo uniqid();?>');"></div>
                </div>
            </div>
        </div>

        <script src="theme/accent/maintenance/vendor/jquery/jquery-3.2.1.min.js"></script>
        <script src="theme/accent/maintenance/vendor/bootstrap/js/popper.js"></script>
        <script src="theme/accent/maintenance/vendor/bootstrap/js/bootstrap.min.js"></script>
        <script src="theme/accent/maintenance/vendor/tilt/tilt.jquery.min.js"></script>
        <script src="theme/accent/maintenance/js/main.js"></script>
    </body>
</html>