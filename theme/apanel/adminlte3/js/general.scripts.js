$(function(){    
    
});

function systemMessages(message, toastrType){
	typeM = typeof message;
    
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }

	switch(typeM){
		case 'string': 
            toastr[toastrType](message);
		break;
		case 'array':
        case 'object': 
            for(var i in message){
                toastr[toastrType](message[i]);
			}
		break;
    }
}

function showLoader(loaderElement, lodaerText){
    var text = undefined === lodaerText ? 'Обработка данных...' : '';

	var $this = $(loaderElement).children('.ajax-loader');

	if ($this.length > 0){
		$this.show();
	} else {
		$(loaderElement).prepend('<div class="ajax-loader">\
                                    <div class="loader-bouces">'+ text +'</div>\
								</div>');
		$(loaderElement).children('.ajax-loader').show();
	}
}

function hideLoader(parentId){
	var $this = $(parentId).children('.ajax-loader');

	if ($this.length > 0)
		$this.hide();
}

function clearSystemMessages(){
    toastr.clear();
}