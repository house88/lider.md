$(function(){    
    $('#js-dashboard-signin-form').on('submit', function(e){
        console.log(siteUrl);
        var fdata = $('#js-dashboard-signin-form').serialize();
		$.ajax({
			type: 'POST',
			url: siteUrl + '/auth/ajax_operations/signin',
			data: fdata,
			dataType: 'JSON',
			beforeSend: function(){
                showLoader('body');
				clearSystemMessages();
			},
			success: function(resp){
                if('success' === resp.mess_type){
                    location.href = resp.redirect;
                } else{
                    systemMessages(resp.message, resp.mess_type);
				    hideLoader('body');
                }
			}
		});
		return false;
    });
});