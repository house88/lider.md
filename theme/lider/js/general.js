"use strict";

//Submenu Dropdown Toggle
if($('.main-header li.dropdown ul').length){
    $('.main-header .navigation li.dropdown').append('<div class="dropdown-btn"><span class="fa fa-angle-right"></span></div>');
}

//Mobile Nav Hide Show
if($('.mobile-menu').length){

    $('.mobile-menu .menu-box').mCustomScrollbar();
    
    //Dropdown Button
    $('.mobile-menu li.dropdown .dropdown-btn').on('click', function() {
        $(this).toggleClass('open');
        $(this).prev('ul').slideToggle(500);
    });
    
    //Dropdown Button
    $('.mobile-menu li.dropdown .dropdown-btn').on('click', function() {
        $(this).prev('.megamenu').slideToggle(900);
    });

    //Menu Toggle Btn
    $('.mobile-nav-toggler').on('click', function() {
        $('body').addClass('mobile-menu-visible');
    });

    //Menu Toggle Btn
    $('.mobile-menu .menu-backdrop,.mobile-menu .close-btn').on('click', function() {
        $('body').removeClass('mobile-menu-visible');
    });
}

//Update Header Style and Scroll to Top
function headerStyle() {
    if($('.main-header').length){
        var windowpos = $(window).scrollTop();
        var siteHeader = $('.main-header');
        var scrollLink = $('.scroll-top');
        if (windowpos >= 350) {
            siteHeader.addClass('fixed-header');
            scrollLink.fadeIn(300);
        } else {
            siteHeader.removeClass('fixed-header');
            scrollLink.fadeOut(300);
        }
    }
}
headerStyle();

jQuery(window).on('scroll', function(){
	(function ($) {
        headerStyle ();
	})(jQuery);
});

// Scroll to a Specific Div
if($('.scroll-to-target').length){
    $(".scroll-to-target").on('click', function(e) {
        e.preventDefault();
        var target = $($(this).attr('data-target'));
		if(target.length){
            var targetOffset = intval($(this).attr('data-offset'));
            var scrollTo = target.offset().top - targetOffset;
            $('body, html').animate({scrollTop: scrollTo+'px'}, 1000);
		}
    });
}

//Hide Loading Box (Preloader)
function handlePreloader() {
    if($('.loader-wrap').length){
        $('.loader-wrap').delay(1000).fadeOut(500);
    }
    TweenMax.to($(".loader-wrap .overlay"), 1.2, {
        force3D: true,
        left: "100%",
        ease: Expo.easeInOut,
    });
}

if ($(".preloader-close").length) {
    $(".preloader-close").on("click", function(){
        $('.loader-wrap').delay(200).fadeOut(500);
    })
}

// Instance Of Fuction while Window Load event
jQuery(window).on('load', function() {
    (function($) {
        handlePreloader ();
    })(jQuery);
});

$(window).enllax();

function intval(num){
	if (typeof num == 'number' || typeof num == 'string'){
		num = num.toString();
		var dotLocation = num.indexOf('.');
		if (dotLocation > 0){
			num = num.substr(0, dotLocation);
		}

		if (isNaN(Number(num))){
			num = parseInt(num);
		}

		if (isNaN(num)){
			return 0;
		}

		return Number(num);
	} else if (typeof num == 'object' && num.length != null && num.length > 0){
		return 1;
	} else if (typeof num == 'boolean' && num === true){
		return 1;
	}

	return 0;
}