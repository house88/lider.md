var map = null;

/**
 * Initialize a map with a given selector and center it on a given location
 * @param selector - The DOM element to which the map will be attached.
 * @param [regionData] - The region data for the map.
 * @returns The map object.
 */
function initMap(selector, regionData = {}) {
    var container = L.DomUtil.get(map);

    //check if the map has already been initialized and is deleted to be reinitialized
    if (container != null) {
        map.remove();
    }

    var centerMapPosition = [47.023, 28.177]; // default center map position Chisinau

    map = L.map(selector, {
        center: centerMapPosition,
        zoom: 1,
        zoomControl: true,
        attributionControl: false,
        // dragging: !L.Browser.mobile,
        // tap: !L.Browser.mobile
    });

    L.control
        .zoom({
            position: "bottomright",
        })
        .addTo(map);

    L.tileLayer(
        "https://i.simpalsmedia.com/map/1/{z}/{x}/{y}.png" + (L.Browser.retina ? "&scale=2" : ""),
        // "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
        {
            detectRetina: true,
            minZoom: !L.Browser.mobile ? 8 : 7,
            maxZoom: 18,
        }
    ).addTo(map);

    // Resize map on init. This is a workaround for a bug in Leaflet.js.
    setTimeout(() => {
        map.invalidateSize();
    }, 500);

    return map;
}

/**
 * It creates markers on the map and popup content for this
 * @param map - the map object
 * @param markersData - an array of objects with the following properties:
 */
function addMarkersOnMap(map) {
    var aIcon = L.icon({
        iconUrl: _baseUrl + "theme/lider/js/plugins/leaflet/images/marker-icon-red.png",
        iconRetinaUrl: _baseUrl + "theme/lider/js/plugins/leaflet/images/marker-icon-red-2x.png",
        shadowUrl: _baseUrl + "theme/lider/js/plugins/leaflet/images/marker-shadow.png",
        iconSize: [25, 41],
        iconAnchor: [12, 41],
        popupAnchor: [-12, -41],
        shadowSize: [41, 41],
    });

    $.each(markersData, function (i, marker) {
        var coordinates = marker.coordinates;
        var position = [coordinates.latitude, coordinates.longitude];

        var mapMarker = L.marker(position, {
            icon: aIcon,
            alt: "map marker-" + marker.id,
            interactive: true,
        }).addTo(map);

        $(mapMarker._icon).addClass(`js-map-marker-icon-${marker.id}`);

        if (marker.content) {
            mapMarker.bindPopup(marker.content, {
                minWidth: 200,
            });
        }
    });
}
