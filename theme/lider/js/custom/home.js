function systemMessages(message, toastrType){
	typeM = typeof message;

    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }

	switch(typeM){
		case 'string':
            toastr[toastrType](message);
		break;
		case 'array':
        case 'object':
            for(var i in message){
                toastr[toastrType](message[i]);
			}
		break;
    }
}


function clearSystemMessages(){
    toastr.clear();
}

//Fact counter
function CounterNumberChanger () {
	var timer = $('.timer');
	if(timer.length) {
		timer.appear(function () {
			timer.countTo();
		})
	}
}

// ===Project===
function projectMasonaryLayout() {
    if ($('.masonary-layout').length) {
        $('.masonary-layout').isotope({
            layoutMode: 'masonry'
        });
    }
}

// Main Slider Carousel
if ($('.banner-carousel').length) {
    $('.banner-carousel').owlCarousel({
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        mouseDrag: false,
        touchDrag: false,
        loop:false,
        margin:0,
        dots: false,
        nav:false,
        singleItem:true,
        smartSpeed: 500,
        autoplay: false,
        autoplayTimeout:6000,
        navText: [ '<span class="flaticon-left-arrow"></span>', '<span class="flaticon-right-arrow"></span>' ],
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1024:{
                items:1
            }
        }
    });
}

//Fact Counter + Text Count
if($('.count-box').length){
    $('.count-box').appear(function(){

        var $t = $(this),
            n = $t.find(".count-text").attr("data-stop"),
            r = parseInt($t.find(".count-text").attr("data-speed"), 10);

        if (!$t.hasClass("counted")) {
            $t.addClass("counted");
            $({
                countNum: $t.find(".count-text").text()
            }).animate({
                countNum: n
            }, {
                duration: r,
                easing: "linear",
                step: function() {
                    $t.find(".count-text").text(Math.floor(this.countNum));
                },
                complete: function() {
                    $t.find(".count-text").text(this.countNum);
                }
            });
        }

    },{accY: 0});
}

//LightBox / Fancybox
if($('.lightbox-image').length) {
    $('.lightbox-image').fancybox({
        openEffect  : 'fade',
        closeEffect : 'fade',

        youtube : {
        controls : 0,
        showinfo : 0
    },

        helpers : {
            media : {}
        }
    });
}

if($('.paroller').length){
    $('.paroller').paroller({
          factor: -0.1,            // multiplier for scrolling speed and offset, +- values for direction control
          factorLg: -0.1,          // multiplier for scrolling speed and offset if window width is less than 1200px, +- values for direction control
          type: 'foreground',     // background, foreground
          direction: 'vertical' // vertical, horizontal
    });
}

if($('.paroller-2').length){
    $('.paroller-2').paroller({
            factor: 0.05,            // multiplier for scrolling speed and offset, +- values for direction control
            factorLg: 0.05,          // multiplier for scrolling speed and offset if window width is less than 1200px, +- values for direction control
            type: 'foreground',     // background, foreground
            direction: 'horizontal' // vertical, horizontal
    });
}

// Elements Animation
if($('.wow').length){
    var wow = new WOW(
      {
        boxClass:     'wow',      // animated element css class (default is wow)
        animateClass: 'animated', // animation css class (default is animated)
        offset:       0,          // distance to the element when triggering the animation (default is 0)
        mobile:       false,       // trigger animations on mobile devices (default is true)
        live:         true       // act on asynchronously loaded content (default is true)
      }
    );
    wow.init();
}

var geoMap = initMap('js-geomap-container');
$(function(){
    addMarkersOnMap(geoMap);

	$('body').on('click touchend', ".call-function", function(e){
		e.preventDefault();
		var $thisBtn = $(this);
		if($thisBtn.hasClass('disabled')){
			return false;
		}

		var callBack = $thisBtn.data('callback');
		window[callBack]($thisBtn);
		return false;
	});

    $('#js-contact-form-1').on('submit', function(e){
        e.preventDefault();
        var $form = $(this);
        $.ajax({
            type: "POST",
            dataType:'json',
            url: _baseLangUrl + 'notify/ajax_operations/callme',
            data: $form.serialize(),
            beforeSend: function(){
                $form.find('button[type="submit"]').prop("disabled", true);
            },
            success: function(resp){
                $form.find('button[type="submit"]').prop("disabled", false);
                systemMessages(resp.message, resp.mess_type);
                if(resp.mess_type == 'success'){
                    $form[0].reset();
                }
            }
        });

        return false;
    });
});


// Dom Ready Function
jQuery(document).on('ready', function () {
	(function ($) {
        CounterNumberChanger ();
	})(jQuery);
});

// Instance Of Fuction while Window Load event
jQuery(window).on('load', function() {
    (function($) {
        projectMasonaryLayout ();
    })(jQuery);
});
