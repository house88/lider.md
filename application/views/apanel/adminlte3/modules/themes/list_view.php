<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-12">
			<div class="card card-widget custom-margin-top-10">
				<div class="card-header">
					<h3 class="card-title"><?php echo $page_header;?></h3>
					<div class="card-tools">
						<a class="btn btn-tool call-popup" data-popup="#general_popup_form" href="#" data-href="<?php echo base_url('admin/themes/popup/setUpdate');?>" tooltip="Добавить шаблон">
							<i class="fas fa-plus"></i> Добавить
						</a>
						<a class="btn btn-tool confirm-dialog" href="#" data-callback="updateThemesConfigs" data-message="Вы уверены что хотите Обновить конфигурации шаблонов?" data-title="Обновить конфигурации" data-type="bg-warning" data-toggle="tooltip" title="Обновить конфигурации">
							<i class="fas fa-sync-alt"></i> Обновить конфигурации
						</a>
					</div>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-bordered table-hover main-data-table" id="dtTable" style="width: 100%;">
							<thead>
								<tr>
									<th class="dt_id">#</th>
									<th class="dt_title">Название</th>
									<th class="dt_views_path">Папка views</th>
									<th class="dt_assets_path">Папка assets</th>
									<th class="dt_default">Базовый</th>
									<!-- <th class="dt_active">Активная</th> -->
									<th class="dt_actions"></th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
