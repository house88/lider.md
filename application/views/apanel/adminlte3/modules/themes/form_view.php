<div class="modal-dialog modal-sm">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">
                <?php echo !empty($themeInfo) ? 'Редактировать' : 'Добавить';?> шаблон
            </h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <form role="form" id="manage_form">
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label>Название</label>
                            <input class="form-control form-control-sm rounded-0" placeholder="Название" name="theme_name" value="<?php echo @$themeInfo['theme_name'];?>">
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label>Папка views</label>
                            <input class="form-control form-control-sm rounded-0" placeholder="ex. public/v1/" name="views_path" value="<?php echo @$themeInfo['theme_views_path'];?>">
                            <span class="text-danger">Путь должен заканчиваться знаком /</span>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label>Папка assets</label>
                            <input class="form-control form-control-sm rounded-0" placeholder="ex. theme/accent/" name="assets_path" value="<?php echo @$themeInfo['theme_assets_path'];?>">
                            <span class="text-danger">Путь должен заканчиваться знаком /</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer d-flex justify-content-end">
                <div class="d-flex">
                    <?php if(!empty($themeInfo)){?>
                        <input type="hidden" name="idTheme" value="<?php echo $themeInfo['id_theme'];?>">
                    <?php }?>
                    <button type="submit" class="btn btn-success btn-flat"><i class="fad fa-check"></i> Сохранить</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script>    
    var manage_form = $('#manage_form');
    manage_form.submit(function () {
        var fdata = manage_form.serialize();
        var $popup_parent = $('#general_popup_form');
        $.ajax({
            type: 'POST',
            url: base_url+'admin/themes/ajax_operations/setUpdate',
            data: fdata,
            dataType: 'JSON',
			beforeSend: function(){
                showLoader('body');
				clearSystemMessages();
			},
            success: function(resp){
                systemMessages(resp.message, resp.mess_type);
                hideLoader('body');
                if(resp.mess_type === 'success'){
                    $popup_parent.modal('hide');
                    dtTable.fnDraw(false);
                }
            }
        });
        return false;
    });
</script>
