<!-- Content Header (Page header) -->
<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-xs-12">
				<h1 class="m-0 text-dark"><?php echo $page_header;?></h1>
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<!-- Main content -->
<section class="content">
	<div class="container-fluid">
		<!-- Small boxes (Stat box) -->
		<div class="row">
			<div class="col-12">
				<form class="form-horizontal" id="settings_form">
					<?php $settings = Modules::run('settings/_getSettings');?>
					<div class="card card-primary card-outline card-tabs">
						<div class="card-header p-0 pt-1 border-bottom-0">
							<ul class="nav nav-tabs" role="tablist">
								<li class="nav-item">
									<a class="nav-link active" id="custom-tabs-settings-ro-tab" data-toggle="pill" href="#custom-tabs-settings-ro" role="tab" aria-controls="custom-tabs-settings-ro" aria-selected="false">RO</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="custom-tabs-settings-ru-tab" data-toggle="pill" href="#custom-tabs-settings-ru" role="tab" aria-controls="custom-tabs-settings-ru" aria-selected="false">RU</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="custom-tabs-settings-en-tab" data-toggle="pill" href="#custom-tabs-settings-en" role="tab" aria-controls="custom-tabs-settings-en" aria-selected="true">EN</a>
								</li>
							</ul>
						</div>
						<div class="card-body">
							<div class="tab-content" id="custom-tabs-two-tabContent">
								<div class="tab-pane fade active show" id="custom-tabs-settings-ro" role="tabpanel" aria-labelledby="custom-tabs-settings-ro-tab">
									<?php foreach($settings as $setting){?>
										<div class="form-group row">
											<label class="col-sm-4 col-form-label"><?php echo $setting['setting_title'];?></label>
											<div class="col-sm-8">
												<input type="text" class="form-control form-control-sm rounded-0" name="settings[<?php echo $setting['setting_alias'];?>][ro]" value="<?php echo $setting['value_ro'];?>">
											</div>
										</div>
									<?php }?>
								</div>
								<div class="tab-pane fade" id="custom-tabs-settings-ru" role="tabpanel" aria-labelledby="custom-tabs-settings-ru-tab">
									<?php foreach($settings as $setting){?>
										<div class="form-group row">
											<label class="col-sm-4 col-form-label"><?php echo $setting['setting_title'];?></label>
											<div class="col-sm-8">
												<input type="text" class="form-control form-control-sm rounded-0" name="settings[<?php echo $setting['setting_alias'];?>][ru]" value="<?php echo $setting['value_ru'];?>">
											</div>
										</div>
									<?php }?>
								</div>
								<div class="tab-pane fade" id="custom-tabs-settings-en" role="tabpanel" aria-labelledby="custom-tabs-settings-en-tab">
									<?php foreach($settings as $setting){?>
										<div class="form-group row">
											<label class="col-sm-4 col-form-label"><?php echo $setting['setting_title'];?></label>
											<div class="col-sm-8">
												<input type="text" class="form-control form-control-sm rounded-0" name="settings[<?php echo $setting['setting_alias'];?>][en]" value="<?php echo $setting['value_en'];?>">
											</div>
										</div>
									<?php }?>
								</div>
							</div>
						</div>
						<div class="card-footer d-flex justify-content-end">
							<button class="btn btn-success btn-flat btn-sm" type="submit"><i class="fad fa-check"></i> Сохранить</button>
						</div>
						<!-- /.card -->
					</div>
				</form>
			</div>
		</div>
		<!-- /.row -->
	</div><!-- /.container-fluid -->
</section>