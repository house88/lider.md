
<script>
	var settings_form = $('#settings_form');
	settings_form.submit(function () {
		var fdata = settings_form.serialize();
		$.ajax({
			type: 'POST',
			url: base_url+'admin/settings/ajax_operations/update',
			data: fdata,
			dataType: 'JSON',
			beforeSend: function(){
                showLoader('body');
				clearSystemMessages();
			},
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
				hideLoader('body');
			}
		});
		return false;
	});
</script>