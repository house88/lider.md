<?php $this->load->view($this->theme->apanel_view('includes/plugins/datatable'));?>

<script>
	var dtTable; //obj of datatable
	$(function(){
		'use strict';
		dtTable = $('#dtTable').dataTable( {
			language: {
				url: "<?php echo file_modification_time($this->theme->apanel_assets('plugins/datatable/js/Russian.json'));?>"
			},
			"sDom": 'ptp',
			"bProcessing": true,
			"bServerSide": true,
			"bSortCellsTop": true,
			"sAjaxSource": base_url + "admin/color_palette/ajax_operations/list",
			"sServerMethod": "POST",
			"iDisplayLength": 100,
			"aoColumnDefs": [
				{ "sClass": "text-center custom-width-50", "aTargets": ["dt_id"], "mData": "dt_id", "bSortable": false},
				{ "sClass": "custom-width-100", "aTargets": ["dt_color"], "mData": "dt_color" , "bSortable": false },
				{ "sClass": "", "aTargets": ["dt_title"], "mData": "dt_title", "bSortable": false},
				{ "sClass": "text-center custom-width-50", "aTargets": ["dt_actions"], "mData": "dt_actions" , "bSortable": false }
			],
			"aaSorting" : [[0,'asc']],
			"fnServerData": function ( sSource, aoData, fnCallback ) {
				$.ajax( {
					"dataType": 'JSON',
					"type": "POST",
					"url": sSource,
					"data": aoData,
					beforeSend: function(){
						showLoader('body');
					},
					"success": function (data, textStatus, jqXHR) {
						if(data.mess_type !== 'success'){
							systemMessages(data.message, data.mess_type);
						}

						fnCallback(data, textStatus, jqXHR);
					}
				});
			},
			"sPaginationType": "full_numbers",
			"fnDrawCallback": function( oSettings ) {
                fixDataTablePagination(this);

				mobileDataTable($('.main-data-table'));
				
                hideLoader('body');
			}
		});
	});

	var delete_action = function(btn){
		var $this = $(btn);
		var id = $this.data('id');
		$.ajax({
			type: 'POST',
			url: base_url+'admin/color_palette/ajax_operations/delete',
			data: {id:id},
			dataType: 'JSON',
			beforeSend: function(){
                showLoader('body');
				clearSystemMessages();
			},
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
				if(resp.mess_type == 'success'){
					dtTable.fnDraw(false);
				}else{
					hideLoader('body');
				}
			}
		});
		return false;
	}	

	var updateThemesConfigs = function(btn){
		var $this = $(btn);
		$.ajax({
			type: 'POST',
			url: base_url+'admin/color_palette/ajax_operations/update_configs',
			data: {},
			dataType: 'JSON',
			beforeSend: function(){
                showLoader('body');
				clearSystemMessages();
			},
			success: function(resp){
				hideLoader('body');
				systemMessages(resp.message, resp.mess_type);
			}
		});
		return false;
	}
</script>