<div class="modal-dialog modal-sm">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">
                <?php echo !empty($colorInfo) ? 'Редактировать' : 'Добавить';?> цвет
            </h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <form role="form" id="manage_form">
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label>Название RO</label>
                            <input class="form-control form-control-sm rounded-0" placeholder="Название RO" name="name_ro" value="<?php echo @$colorInfo['name_ro'];?>">
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label>Название RU</label>
                            <input class="form-control form-control-sm rounded-0" placeholder="Название RU" name="name_ru" value="<?php echo @$colorInfo['name_ru'];?>">
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label>Название EN</label>
                            <input class="form-control form-control-sm rounded-0" placeholder="Название EN" name="name_en" value="<?php echo @$colorInfo['name_en'];?>">
                        </div>
                    </div>
                    <div class="col-12">
                        <!-- Color Picker -->
                        <div class="form-group">
                            <label>Цвет</label>

                            <div class="input-group" id="js-hex-code-colorpicker">
                                <input type="text" class="form-control form-control-sm rounded-0" name="hex_code" value="<?php echo @$colorInfo['hex_code'];?>">

                                <div class="input-group-append">
                                    <?php $defaultColor = !empty($colorInfo) ? $colorInfo['hex_code'] : '#495057';?>
                                    <span class="input-group-text"><i class="fas fa-square" style="color:<?php echo $defaultColor;?>"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label>Образец Матовый</label>
                            <div class="input-group input-group-sm">
                                <input id="js-photo-sample-matt" class="form-control form-control-sm rounded-0" name="sample_matt" value="<?php echo @$colorInfo['sample_image_matt'];?>">
                                <span class="input-group-append">
                                    <button type="button" class="btn btn-default btn-sm btn-flat flex-fill call-function" data-callback="openFilemanager" data-src="<?php echo site_url('tinymanager/init/default');?>/dialog.php?type=1&popup=1&field_id=js-photo-sample-matt&relative_url=1">
                                        <i class="fad fa-image"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label>Образец Глянец</label>
                            <div class="input-group input-group-sm">
                                <input id="js-photo-sample-gloss" class="form-control form-control-sm rounded-0" name="sample_gloss" value="<?php echo @$colorInfo['sample_image_gloss'];?>">
                                <span class="input-group-append">
                                    <button type="button" class="btn btn-default btn-sm btn-flat flex-fill call-function" data-callback="openFilemanager" data-src="<?php echo site_url('tinymanager/init/default');?>/dialog.php?type=1&popup=1&field_id=js-photo-sample-gloss&relative_url=1">
                                        <i class="fad fa-image"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer d-flex justify-content-end">
                <div class="d-flex">
                    <?php if(!empty($colorInfo)){?>
                        <input type="hidden" name="idColor" value="<?php echo $colorInfo['id'];?>">
                    <?php }?>
                    <button type="submit" class="btn btn-success btn-flat"><i class="fad fa-check"></i> Сохранить</button>
                </div>
            </div>
        </form>
    </div>
</div>
<?php $this->load->view($this->theme->apanel_view('includes/plugins/colorpicker'));?>
<script>
    $('#js-hex-code-colorpicker').colorpicker()
    .on('colorpickerChange', function(event) {
        $('#js-hex-code-colorpicker .fa-square').css('color', event.color.toString());
    });

    var manage_form = $('#manage_form');
    manage_form.submit(function () {
        var fdata = manage_form.serialize();
        var $popup_parent = $('#general_popup_form');
        $.ajax({
            type: 'POST',
            url: base_url+'admin/color_palette/ajax_operations/setUpdate',
            data: fdata,
            dataType: 'JSON',
			beforeSend: function(){
                showLoader('body');
				clearSystemMessages();
			},
            success: function(resp){
                systemMessages(resp.message, resp.mess_type);
                hideLoader('body');
                if(resp.mess_type === 'success'){
                    $popup_parent.modal('hide');
                    dtTable.fnDraw(false);
                }
            }
        });
        return false;
    });
</script>
