<?php $this->load->view($this->theme->apanel_view('includes/plugins/datatable'));?>
<?php $this->load->view($this->theme->apanel_view('includes/plugins/daterangepicker'));?>

<script>
	var dtTable; //obj of datatable
	var dtFilter;
	var dt_params = [
		{ "sClass": "vam text-center custom-width-50", "aTargets": ["dt_id"], "mData": "dt_id"},
		{ "sClass": "vam text-left", "aTargets": ["dt_name"], "mData": "dt_name"},
		{ "sClass": "vam text-left custom-width-150 d-none d-xl-table-cell", "aTargets": ["dt_phone"], "mData": "dt_phone", "bSortable": false},
		{ "sClass": "vam text-center custom-width-100 d-none d-xl-table-cell", "aTargets": ["dt_blocked"], "mData": "dt_blocked", "bSortable": false},
		{ "sClass": "vam text-center custom-width-120", "aTargets": ["dt_regdate"], "mData": "dt_regdate"},
		{ "sClass": "vam text-center custom-width-50", "aTargets": ["dt_actions"], "mData": "dt_actions" , "bSortable": false }
	];		
	<?php if(isset($is_staff) && $is_staff === true){?>
		dt_params.push({ "sClass": "vam text-left custom-width-150", "aTargets": ["dt_type"], "mData": "dt_type"});
	<?php }?>
	
	$(function(){
		'use strict';
		dtTable = $('#dtTable').dataTable( {
			language: {
				url: "<?php echo file_modification_time($this->theme->apanel_assets('plugins/datatable/js/Russian.json'));?>"
			},
			"sDom": 'ptp',
			"bProcessing": true,
			"bServerSide": true,
			"bSortCellsTop": true,
			"sAjaxSource": base_url + "admin/users/ajax_operations/list<?php if(isset($is_staff) && $is_staff === true){echo '/staff';}?>",
			"sServerMethod": "POST",
			"iDisplayLength": 100,
			"aoColumnDefs": dt_params,
			"aaSorting" : [[0,'desc']],
			"fnServerData": function ( sSource, aoData, fnCallback ) {
				if(!dtFilter){
					dtFilter = initDtFilter('orders');
				}

				aoData = aoData.concat(dtFilter.getDTFilter());

				$.ajax( {
					"dataType": 'JSON',
					"type": "POST",
					"url": sSource,
					"data": aoData,
					beforeSend: function(){
						showLoader('body');
					},
					"success": function (data, textStatus, jqXHR) {
						if(data.mess_type !== 'success'){
							systemMessages(data.message, data.mess_type);
						}

						$('#total_dtTable-counter').text(data.iTotalDisplayRecords);

						fnCallback(data, textStatus, jqXHR);
					}
				});
			},
			"sPaginationType": "full_numbers",
			"fnDrawCallback": function( oSettings ) {
                fixDataTablePagination(this);

				mobileDataTable($('.main-data-table'));
				
                hideLoader('body');
			}
		});

		$('#js-user-registered-date').daterangepicker({
			timePicker: false,
			parentEl: '.modal',
			locale: {
				format: 'DD.MM.YYYY'
			},
			autoUpdateInput: false,
			"autoApply": true,
			"opens": "left",
    		"drops": "down"
		}).on('apply.daterangepicker', function(ev, picker) {
			$(this).val(picker.startDate.format('DD.MM.YYYY') + ' - ' + picker.endDate.format('DD.MM.YYYY')).trigger('change');
		}).on('cancel.daterangepicker', function(ev, picker) {
			$(this).val('').trigger('change');
		});
	});

	var change_status = function(element){
		var $this = $(element);
		var user = $this.data('user');
		$.ajax({
			type: 'POST',
			url: base_url+'admin/users/ajax_operations/change_status',
			data: {user:user},
			dataType: 'JSON',
			beforeSend: function(){
                showLoader('body');
				clearSystemMessages();
			},
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
				if(resp.mess_type == 'success'){
					dtTable.fnDraw(false);
				}
			}
		});
		return false;
	}
</script>