<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">
                <?php echo !empty($user) ? 'Редактировать' : 'Добавить';?> пользователя
            </h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>

        <form role="form" id="manage_form">
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label>Имя</label>
                            <input class="form-control form-control-sm rounded-0" placeholder="Имя" name="user_name" value="<?php echo !empty($user) ? $user->user_login : '';?>">
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label>Email</label>
                            <input class="form-control form-control-sm rounded-0" placeholder="Email" name="user_email" value="<?php echo !empty($user) ? $user->user_email : '';?>">
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label>Телефон</label>
                            <input class="form-control form-control-sm rounded-0" placeholder="Телефон" name="user_phone" value="<?php echo !empty($user) ? $user->user_phone : '';?>">
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label>Адрес</label>
                            <input class="form-control form-control-sm rounded-0" placeholder="Адрес" name="user_address" value="<?php echo $user->user_address ?? '';?>">
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label>Группа</label>
                            <select name="group" class="form-control form-control-sm rounded-0">
                                <option>Выберите группу</option>
                                <?php foreach($groups as $group){?>
                                    <option value="<?php echo $group['id_group'];?>" data-type="<?php echo $group['group_type'];?>" <?php echo get_choice('selected', !empty($user) && $group['id_group'] === $user->id_group);?>><?php echo $group['group_name'];?></option>
                                <?php }?>
                            </select>
                        </div>
                    </div>
                    <div class="col-12">
                        <?php if(!empty($user)){?>
                            <div class="form-group">
                                <div class="icheck-primary">
                                    <input name="change_password" type="checkbox" id="js-checkbox-user-change-password">
                                    <label for="js-checkbox-user-change-password">
                                        Сменить пароль
                                    </label>
                                </div>
                            </div>
                        <?php }?>
                        <div class="js-change-password-fields" <?php echo get_choice('style="display:none;"', !empty($user));?>>
                            <div class="form-group">
                                <label>Пароль</label>
                                <input class="form-control form-control-sm rounded-0" type="password" placeholder="Пароль" name="user_pass" value="">
                            </div>
                            <div class="form-group">
                                <label>Повторите пароль</label>
                                <input class="form-control form-control-sm rounded-0" type="password" placeholder="Повторите пароль" name="confirm_user_pass" value="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer d-flex justify-content-between">
                <div class="icheck-primary">
                    <input name="status" type="checkbox" id="js-checkbox-user-active" <?php echo get_choice('checked', !empty($user) ? (int) $user->status === 1 : false);?>>
                    <label for="js-checkbox-user-active">
                        Email подтвержден
                    </label>
                </div>
                <div class="d-flex">
                    <?php if(!empty($user)){?>
                        <input type="hidden" name="user" value="<?php echo $user->id;?>">
                    <?php }?>
                    <button type="submit" class="btn btn-success btn-flat"><i class="fad fa-check"></i> Сохранить</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    var manage_form = $('#manage_form');
    manage_form.submit(function () {
        var fdata = manage_form.serialize();
        var $popup_parent = $('#general_popup_form');
        $.ajax({
            type: 'POST',
            url: base_url+'admin/users/ajax_operations/<?php echo !empty($user) ? 'edit' : 'add';?>',
            data: fdata,
            dataType: 'JSON',
			beforeSend: function(){
                showLoader('body');
				clearSystemMessages();
			},
            success: function(resp){
                systemMessages(resp.message, resp.mess_type);
                hideLoader('body');
                if(resp.mess_type === 'success'){
                    $popup_parent.modal('hide');
                    dtTable.fnDraw(false);
                }
            }
        });
        return false;
    });


    $(".bs-select-categories").selectpicker();

    $('select[name="group"]').on('change', function(){
        var group_type = $(this).find('option:selected').data('type');
        if(group_type === 'content_menedger'){
            $('.js-form-user-categories').show();
        } else{
            $('.js-form-user-categories').hide();
        }
    });

    $('input[name="change_password"]').on('change', function(e){
        if($(this).prop('checked') === true){
            $('.js-change-password-fields').show();
        } else{
            $('.js-change-password-fields').hide();
        }
    });
</script>
