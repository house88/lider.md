<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-12">
			<div class="card card-widget custom-margin-top-10">
				<div class="card-header">
					<h3 class="card-title">
						<?php echo $page_header;?>, <small>найдено: <span id="total_dtTable-counter">0</span></small>
					</h3>
					<div class="card-tools">
						<a class="btn btn-tool call-popup" data-popup="#general_popup_form" href="#" data-href="<?php echo base_url('admin/groups/popup/add');?>" tooltip="Добавить группу">
							<i class="fas fa-plus"></i> Добавить
						</a>
					</div>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-bordered table-hover main-data-table" id="dtTable" style="width: 100%;">
							<thead>
								<tr>
									<th class="dt_id">#</th>
									<th class="dt_name">Имя</th>
									<th class="dt_type">Тип</th>
									<th class="dt_actions"></th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
