<?php $this->load->view($this->theme->apanel_view('includes/plugins/datatable'));?>
<?php $this->load->view($this->theme->apanel_view('includes/plugins/colorpicker'));?>

<script>
	var dtTable;
	$(function(){
		'use strict';
		dtTable = $('#dtTable').dataTable( {
			language: {
				url: "<?php echo file_modification_time($this->theme->apanel_assets('plugins/datatable/js/Russian.json'));?>"
			},
			"sDom": 'ptp',
			"bProcessing": true,
			"bServerSide": true,
			"bSortCellsTop": true,
			"sAjaxSource": base_url + "admin/groups/ajax_operations/list",
			"sServerMethod": "POST",
			"iDisplayLength": 50,
			"aoColumnDefs": [
				{ "sClass": "van text-center custom-width-50", "aTargets": ["dt_id"], "mData": "dt_id"},
				{ "sClass": "vam", "aTargets": ["dt_name"], "mData": "dt_name"},
				{ "sClass": "vam text-center custom-width-200", "aTargets": ["dt_type"], "mData": "dt_type"},
				{ "sClass": "vam text-center custom-width-50", "aTargets": ["dt_actions"], "mData": "dt_actions" , "bSortable": false }
			],
			"aaSorting" : [],
			"fnServerData": function ( sSource, aoData, fnCallback ) {
				$.ajax( {
					"dataType": 'JSON',
					"type": "POST",
					"url": sSource,
					"data": aoData,
					beforeSend: function(){
						showLoader('body');
					},
					"success": function (data, textStatus, jqXHR) {
						if(data.mess_type !== 'success'){
							systemMessages(data.message, data.mess_type);
							
						}
						
						$('#total_dtTable-counter').text(data.iTotalDisplayRecords);

						fnCallback(data, textStatus, jqXHR);
					}
				});
			},
			"sPaginationType": "full_numbers",
			"fnDrawCallback": function( oSettings ) {
                fixDataTablePagination(this);

				mobileDataTable($('.main-data-table'));
				
                hideLoader('body');
			}
		});
	});

	var delete_action = function(btn){
		var $this = $(btn);
		var group = $this.data('group');
		$.ajax({
			type: 'POST',
			url: base_url+'admin/groups/ajax_operations/delete',
			data: {group:group},
			dataType: 'JSON',
			beforeSend: function(){
				showLoader('body');
				clearSystemMessages();
			},
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
				if(resp.mess_type == 'success'){
					dtTable.fnDraw(false);
				}
			}
		});
		return false;
	}
</script>