<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Авторизация</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?php echo file_modification_time($this->theme->apanel_assets('plugins/fontawesome-pro/css/all.min.css'));?>">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?php echo file_modification_time($this->theme->apanel_assets('css/adminlte.min.css'));?>">
        <!-- Toastr -->
        <link rel="stylesheet" href="<?php echo file_modification_time($this->theme->apanel_assets('plugins/toastr/toastr.min.css'));?>">
        <!-- Style -->
        <link rel="stylesheet" href="<?php echo file_modification_time($this->theme->apanel_assets('css/style.css'));?>">
        <!-- Sizes -->
        <link rel="stylesheet" href="<?php echo file_modification_time($this->theme->apanel_assets('css/custom_sizes.css'));?>">
        <!-- Google Font: Source Sans Pro -->
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

        <!-- Favicon -->
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo file_modification_time($this->theme->apanel_assets('favicon/apple-touch-icon.png'));?>">
        <link rel="icon" type="image/png" href="<?php echo file_modification_time($this->theme->apanel_assets('favicon/favicon-32x32.png'));?>" sizes="32x32">
        <link rel="icon" type="image/png" href="<?php echo file_modification_time($this->theme->apanel_assets('favicon/favicon-16x16.png'));?>" sizes="16x16">
    </head>
    <body class="hold-transition login-page">
        <div class="login-box">
            <div class="login-logo">
                <strong>Панель</strong> управления
            </div>
            <!-- /.login-logo -->
            <div class="card">
                <div class="card-body login-card-body">
                    <form id="js-dashboard-signin-form">
                        <div class="input-group mb-3">
                            <input type="email" name="email" class="form-control" placeholder="Email">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-envelope"></span>
                                </div>
                            </div>
                        </div>
                        <div class="input-group mb-3">
                            <input type="password" name="password" class="form-control" placeholder="Password">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-lock"></span>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary btn-block">Sign In</button>
                    </form>
                </div>
                <!-- /.login-card-body -->
            </div>
        </div>
        
        <script>
            Object.defineProperties(window, {
                "siteUrl": {
                    writable: false,
                    value: "<?php echo site_url($this->lang->lang());?>"
                }
            });
        </script>
        <!-- jQuery -->
        <script src="<?php echo file_modification_time($this->theme->apanel_assets('plugins/jquery/jquery.min.js'));?>"></script>
        <!-- Bootstrap 4 -->
        <script src="<?php echo file_modification_time($this->theme->apanel_assets('plugins/bootstrap/js/bootstrap.bundle.min.js'));?>"></script>
        <!-- Toastr -->
        <script src="<?php echo file_modification_time($this->theme->apanel_assets('plugins/toastr/toastr.min.js'));?>"></script>

        <!-- AdminLTE App -->
        <script src="<?php echo file_modification_time($this->theme->apanel_assets('js/adminlte.js'));?>"></script>
        <script src="<?php echo file_modification_time($this->theme->apanel_assets('js/general.scripts.js'));?>"></script>
        <script src="<?php echo file_modification_time($this->theme->apanel_assets('js/auth.scripts.js'));?>"></script>

        <?php if(!empty($flash_message)){?>
            <script>
                var flash_message = JSON.parse('<?php echo $flash_message;?>');
                systemMessages(flash_message.message, flash_message.type);
            </script>
        <?php }?>
    </body>

</html>
