<!DOCTYPE html>
<html>
    <head>
        <?php $this->load->view($this->theme->apanel_view('includes/header'));?>
    </head>
    <body class="hold-transition sidebar-mini layout-fixed text-sm">
        <div class="wrapper">
            <?php $this->load->view($this->theme->apanel_view('includes/navigation'));?>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <?php $this->load->view($main_content);?>
                <!-- /.content -->
            </div>

            <!-- Control Sidebar -->
            <aside class="control-sidebar control-sidebar-dark">
                <!-- Control sidebar content goes here -->
            </aside>
            <!-- /.control-sidebar -->
        </div>
        <!-- ./wrapper -->

        <?php $this->load->view($this->theme->apanel_view('includes/global_scripts'));?>
    </body>

</html>
