<script src="<?php echo file_modification_time($this->theme->apanel_assets('plugins/fileupload/jquery.ui.widget.js'));?>"></script>
<script src="<?php echo file_modification_time($this->theme->apanel_assets('plugins/fileupload/jquery.fileupload.js'));?>"></script>
<script>
    var remove_photo = function(btn){
        var $this = $(btn);
        var photo = $this.data('photo');
        $this.closest('form').append('<input type="hidden" name="remove_photos[]" value="'+photo+'"/>');
        $this.closest('.user-image-thumbnail-wr').remove();
    }
</script>