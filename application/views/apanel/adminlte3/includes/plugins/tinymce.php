<script src="<?php echo file_modification_time($this->theme->apanel_assets('plugins/tinymce/tinymce.min.js'));?>"></script>
<script>
    function initTinyMce(selector){
        var tinymce_selector = undefined !== selector ? selector : '.description';
        tinymce.init({
            relative_urls: false,
            selector: tinymce_selector,
            height: 300,
            plugins: [
                "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                "save table contextmenu directionality emoticons template powerpaste textcolor responsivefilemanager"
            ],
            powerpaste_word_import: "prompt",
            powerpaste_html_import: "prompt",
            toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | fontsizeselect | styleselect",
            toolbar2: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | print preview media fullpage ",
            image_advtab: true ,
            fontsize_formats: '8px 10px 12px 14px 16px 18px 20px 22px 24px 36px',
            external_filemanager_path:"/tinymanager/init/default/",
            filemanager_title:"Responsive Filemanager" ,
            external_plugins: { "filemanager" : "/application/libraries/TinyManager/plugin.min.js"}
        });
    }

    function initTinyMceWithCustomElements(selector){
        var tinymce_selector = undefined !== selector ? selector : '.description';
        tinymce.init({
            relative_urls: false,
            selector: tinymce_selector,
            height: 300,
            plugins: [
                "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                "save table contextmenu directionality emoticons template textcolor responsivefilemanager"
            ],
            toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | fontsizeselect | styleselect",
            toolbar2: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | print preview media fullpage ",
            image_advtab: true ,
            extended_valid_elements:"style,link[href|rel],script[language|type|src]",
            custom_elements:"style,script,link,~link",
            fontsize_formats: '8px 10px 12px 14px 16px 18px 20px 22px 24px 36px',
            external_filemanager_path:"/tinymanager/init/default/",
            filemanager_title:"Responsive Filemanager" ,
            external_plugins: { "filemanager" : "/application/libraries/TinyManager/plugin.min.js"}
        });
    }
</script>
