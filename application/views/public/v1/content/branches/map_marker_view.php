<div class="marker-location-wrapper">
    <div class="marker-location-city-name">LIDER <?php echo $this->lang->getI18nColumn('city_name', $geoBranch);?></div>
    <div class="marker-location-address"><?php echo $this->lang->getI18nColumn('address', $geoBranch);?></div>
    <div class="marker-location-phone">
        <?php $geoBranchPhones = json_decode($geoBranch['phones'], true);?>
        <?php foreach($geoBranchPhones as $geoBranchPhone){?>
            <a href="tel:<?php echo $geoBranchPhone;?>"><?php echo $geoBranchPhone;?></a>
        <?php }?>
    </div>
</div>