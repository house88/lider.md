<script src="<?php echo $this->theme->public_assets('js/isotope.js', true);?>"></script>
<script src="<?php echo $this->theme->public_assets('js/jquery.countTo.js', true);?>"></script>
<script src="<?php echo $this->theme->public_assets('js/jquery.fancybox.js', true);?>"></script>
<script src="<?php echo $this->theme->public_assets('js/jquery.paroller.min.js', true);?>"></script>
<script src="<?php echo $this->theme->public_assets('js/owl.js', true);?>"></script>
<script src="<?php echo $this->theme->public_assets('js/TweenMax.min.js', true);?>"></script>
<script src="<?php echo $this->theme->public_assets('js/validation.js', true);?>"></script>
<script src="<?php echo $this->theme->public_assets('js/wow.js', true);?>"></script>
<link href="<?php echo $this->theme->public_assets('js/plugins/leaflet/leaflet.css');?>" rel="stylesheet">
<script src="<?php echo $this->theme->public_assets('js/plugins/leaflet/leaflet.js');?>"></script>
<script src="<?php echo $this->theme->public_assets('js/geo-map.js'); ?>"></script>

<!-- thm custom script -->
<script src="<?php echo $this->theme->public_assets('js/custom/home.js', true);?>"></script>

<div class="modal" id="orderCallbackModal" tabindex="-1">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Заявка принята</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p>Мы получили информацию. Скоро с вами свяжется наш специалист.</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
			</div>
		</div>
	</div>
</div>