<section class="breadcrumb-area">
    <div class="breadcrumb-area-bg" style="background-image: url(<?php echo $this->theme->public_assets('images/breadcrumb/breadcrumb-1.jpg');?>);">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="inner-content">
                    <div class="title">
                        <h2>Калькулятор расчета кровли</h2>
                    </div>
                    <div class="breadcrumb-menu">
                        <ul>
                            <li><a href="<?php echo site_url();?>"><?php echo $this->lang->line('homePageLinkText');?></a></li>
                            <li><i class="fa fa-angle-right" aria-hidden="true"></i></li>
                            <li class="active">Расчет кровли</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Start Service Style2 Area-->
<section class="roofcalc-style-area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                Здесь будет калькулятор расчета кровли...
            </div>
        </div>
    </div>
</section>
<!--End Service Style2 Area-->

<!--Start Contact Style1 Area-->
<section class="contact-style1-area" id="js-order-callback-section">
    <div class="shape1">
        <img class="paroller-2" src="theme/lider/images/shape/contact-style1-shape-1.png" alt="">
    </div>
    <div class="shape2">
        <img class="zoom-fade" src="theme/lider/images/shape/contact-style1-shape-2.png" alt="">
    </div>
    <div class="shape3 float-bob-y" style="background-image: url(theme/lider/images/shape/contact-style1-shape-3.png);"></div>
    <div class="container">
        <div class="row">
            <div class="col-xl-4">
                <div class="contact-style1__image wow slideInLeft" data-wow-delay="100ms"
                    data-wow-duration="2500ms">
                    <img src="theme/lider/images/resources/house-1.png" alt="">
                </div>
            </div>
            <div class="col-xl-8">
                <div class="contact-form-box1">
                    <div class="top-title">
                        <h2>Нужна помощь<br> специалиста?</h2>
                    </div>
                    <form id="contact-form-1" name="contact_form" class="default-form1" action="#"
                        method="post">
                        <div class="row">
                            <div class="col-xl-4">
                                <div class="input-box">
                                    <input type="text" name="form_name" value="" placeholder="Имя">
                                </div>
                            </div>
                            <div class="col-xl-4">
                                <div class="input-box">
                                    <input type="text" name="form_phone" value="" placeholder="Телефон">
                                </div>
                            </div>
                            <div class="col-xl-4">
                                <div class="button-box">
                                    <button class="btn-one style2" type="button" data-toggle="modal" data-target="#orderCallbackModal">
                                        <span class="txt">Позвоните мне</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<!--End Contact Style1 Area-->

<!--Start Locations Area-->
<section class="locations-section-area" id="js-locations-section">
    <div class="container">
        <div class="row">
            <div class="col-12 col-xl-6">
                <div class="section-area-1">
                    <div class="sec-title text-center">
                        <h2>Наши филиалы</h2>
                    </div>
                    <div class="row">
                        <?php $geoBranches = Modules::run('branches/getListI18n');?>
                        <?php foreach($geoBranches as $geoBranch){?>
                            <div class="col-12 col-sm-6 col-lg-4 col-xl-6">
                                <div class="location-wrapper">
                                    <div class="location-city-name"><?php echo $geoBranch['name'];?></div>
                                    <div class="location-address"><?php echo $geoBranch['address'];?></div>
                                    <div class="location-phone">
                                        <?php foreach($geoBranch['phones'] as $geoBranchPhone){?>
                                            <a href="tel:<?php echo $geoBranchPhone;?>"><?php echo $geoBranchPhone;?></a>
                                        <?php }?>
                                    </div>
                                </div>
                            </div>
                        <?php }?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-area-2 d-none d-xl-block">
        <div class="locations-geomap-container" id="js-geomap-container"></div>
    </div>
    <script>
        var markersData = <?php echo json_encode($geoBranches);?>;
    </script>
</section>
<!--End Locations Area-->
