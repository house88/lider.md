<!-- Start Main Slider -->
<section class="main-slider style2">
    <div class="slider-box">
        <div class="banner-carousel owl-theme owl-carousel">

            <!-- Slide -->
            <div class="slide">
                <div class="shape1 wow slideInLeft" data-wow-delay="100ms" data-wow-duration="1000ms">
                    <img class="float-bob-y" src="<?php echo $this->theme->public_assets('images/shape/slide-style2-shape-1.png');?>" alt="">
                </div>
                <div class="shape2 wow slideInDown" data-wow-delay="100ms" data-wow-duration="1000ms">
                    <img class="float-bob-x" src="<?php echo $this->theme->public_assets('images/shape/slide-style2-shape-2.png');?>" alt="">
                </div>
                <div class="shape3 wow slideInLeft" data-wow-delay="100ms" data-wow-duration="1000ms">
                    <img class="rotate-me" src="<?php echo $this->theme->public_assets('images/shape/slide-style2-shape-3.png');?>" alt="">
                </div>
                <div class="shape4 wow zoomIn" data-wow-duration="1000ms">
                    <img class="zoom-fade" src="<?php echo $this->theme->public_assets('images/shape/slide-style2-shape-4.png');?>" alt="">
                </div>
                <div class="auto-container">
                    <div class="content">
                        <div class="big-title">
                            <h2><?php echo $this->lang->line('mainSliderTitle');?></h2>
                        </div>
                        <div class="text">
                            <p>
                                <span class="fa fa-check"></span> <?php echo $this->lang->line('mainSliderSubTitleOne');?>
                            </p>
                            <p>
                                <span class="fa fa-check"></span> <?php echo $this->lang->line('mainSliderSubTitleTwo');?>
                            </p>
                            <p>
                                <span class="fa fa-check"></span> <?php echo $this->lang->line('mainSliderSubTitleThree');?>
                            </p>
                            <p>
                                <span class="fa fa-check"></span> <?php echo $this->lang->line('mainSliderSubTitleFour');?>
                            </p>
                        </div>
                        <div class="btns-box">
                            <a class="btn-one" href="<?php echo site_url('roofcalc');?>">
                                <span class="txt"><?php echo $this->lang->line('calculatorPageLinkText');?></span>
                            </a>
                        </div>
                    </div>
                    <div class="slide-right-box">
                        <div class="slider-right-box-bg" style="background-image: url(<?php echo $this->theme->public_assets('images/slides/house-1.png', true);?>);"></div>
                        <!-- <div class="video-holder-box1">
                            <div class="icon wow zoomIn" data-wow-delay="300ms" data-wow-duration="1500ms">
                                <span>
                                    <span class="fa fa-star"></span>
                                </span>
                            </div>
                            <div class="title">
                                <h4><?php echo $this->lang->line('mainSliderGuaranty');?></h4>
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<!-- End Main Slider -->

<!--Start Service Style2 Area-->
<section class="service-style2-area" id="js-products-section">
    <div class="container">
        <div class="sec-title text-center">
            <div class="sub-title">
                <p><?php echo $this->lang->line('productsSectionTitle')?></p>
            </div>
            <h2><?php echo $this->lang->line('productsSectionSubTitle')?></h2>
        </div>
        <div class="row">
            <!--Start Single Service Style1-->
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 col-xl-4">
                <div class="single-service-style1 text-center wow fadeInUp" data-wow-delay="00ms" data-wow-duration="1500ms">
                    <div class="img-holder">
                        <div class="inner">
                            <div class="inner-active">
                                <a class="btn-one style-black" href="#">
                                    <span class="txt"><?php echo $this->lang->line('generalButtonViewDetailsText');?></span>
                                </a>
                            </div>
                            <img src="<?php echo $this->theme->public_assets('images/services/service-v1-1.jpg');?>" alt="">
                        </div>
                        <div class="shape-bg" style="background-image: url(<?php echo $this->theme->public_assets('images/shape/service-image-shape.png', true);?>);"></div>
                    </div>
                    <div class="title-holder">
                        <h3><?php echo $this->lang->line('productsSectionCategoryMetalTile');?></h3>
                        <div class="inner-text">
                            <p><?php echo $this->lang->line('productsSectionCategoryMetalTileShortDescription');?></p>
                        </div>
                    </div>
                </div>
            </div>
            <!--End Single Service Style1-->
            <!--Start Single Service Style1-->
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 col-xl-4">
                <div class="single-service-style1 text-center wow fadeInUp" data-wow-delay="200ms" data-wow-duration="1500ms">
                    <div class="img-holder">
                        <div class="inner">

                            <div class="inner-active">
                                <a class="btn-one style-black" href="#">
                                    <span class="txt"><?php echo $this->lang->line('generalButtonViewDetailsText');?></span>
                                </a>
                            </div>
                            <img src="<?php echo $this->theme->public_assets('images/services/service-v1-2.jpg');?>" alt="">
                        </div>
                        <div class="shape-bg" style="background-image: url(<?php echo $this->theme->public_assets('images/shape/service-image-shape.png', true);?>);"></div>
                    </div>
                    <div class="title-holder">
                        <h3><?php echo $this->lang->line('productsSectionCategoryDrainageSystem');?></h3>
                        <div class="inner-text">
                            <p><?php echo $this->lang->line('productsSectionCategoryDrainageSystemShortDescription');?></p>
                        </div>
                    </div>
                </div>
            </div>
            <!--End Single Service Style1-->
            <!--Start Single Service Style1-->
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 col-xl-4">
                <div class="single-service-style1 text-center wow fadeInUp" data-wow-delay="400ms" data-wow-duration="1500ms">
                    <div class="img-holder">
                        <div class="inner">

                            <div class="inner-active">
                                <a class="btn-one style-black" href="#">
                                    <span class="txt"><?php echo $this->lang->line('generalButtonViewDetailsText');?></span>
                                </a>
                            </div>
                            <img src="<?php echo $this->theme->public_assets('images/services/service-v1-3.jpg');?>" alt="">
                        </div>
                        <div class="shape-bg" style="background-image: url(<?php echo $this->theme->public_assets('images/shape/service-image-shape.png', true);?>);"></div>
                    </div>
                    <div class="title-holder">
                        <h3><?php echo $this->lang->line('productsSectionCategoryFences');?></h3>
                        <div class="inner-text">
                            <p><?php echo $this->lang->line('productsSectionCategoryFencesShortDescription');?></p>
                        </div>
                    </div>
                </div>
            </div>
            <!--End Single Service Style1-->
            <!--Start Single Service Style1-->
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 col-xl-4">
                <div class="single-service-style1 text-center wow fadeInUp" data-wow-delay="400ms" data-wow-duration="1500ms">
                    <div class="img-holder">
                        <div class="inner">

                            <div class="inner-active">
                                <a class="btn-one style-black" href="#">
                                    <span class="txt"><?php echo $this->lang->line('generalButtonViewDetailsText');?></span>
                                </a>
                            </div>
                            <img src="<?php echo $this->theme->public_assets('images/services/service-v1-4.jpg');?>" alt="">
                        </div>
                        <div class="shape-bg" style="background-image: url(<?php echo $this->theme->public_assets('images/shape/service-image-shape.png', true);?>);"></div>
                    </div>
                    <div class="title-holder">
                        <h3><?php echo $this->lang->line('productsSectionCategoryProfiledFlooring');?></h3>
                        <div class="inner-text">
                            <p><?php echo $this->lang->line('productsSectionCategoryProfiledFlooringShortDescription');?></p>
                        </div>
                    </div>
                </div>
            </div>
            <!--End Single Service Style1-->
            <!--Start Single Service Style1-->
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 col-xl-4">
                <div class="single-service-style1 text-center wow fadeInUp" data-wow-delay="400ms" data-wow-duration="1500ms">
                    <div class="img-holder">
                        <div class="inner">

                            <div class="inner-active">
                                <a class="btn-one style-black" href="#">
                                    <span class="txt"><?php echo $this->lang->line('generalButtonViewDetailsText');?></span>
                                </a>
                            </div>
                            <img src="<?php echo $this->theme->public_assets('images/services/service-v1-5.jpg');?>" alt="">
                        </div>
                        <div class="shape-bg" style="background-image: url(<?php echo $this->theme->public_assets('images/shape/service-image-shape.png', true);?>);"></div>
                    </div>
                    <div class="title-holder">
                        <h3><?php echo $this->lang->line('productsSectionCategoryAdditionalElements');?></h3>
                        <div class="inner-text">
                            <p><?php echo $this->lang->line('productsSectionCategoryAdditionalElementsShortDescription');?></p>
                        </div>
                    </div>
                </div>
            </div>
            <!--End Single Service Style1-->
            <!--Start Single Service Style1-->
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 col-xl-4">
                <div class="single-service-style1 text-center wow fadeInUp" data-wow-delay="400ms" data-wow-duration="1500ms">
                    <div class="img-holder">
                        <div class="inner">

                            <div class="inner-active">
                                <a class="btn-one style-black" href="#">
                                    <span class="txt"><?php echo $this->lang->line('generalButtonViewDetailsText');?></span>
                                </a>
                            </div>
                            <img src="<?php echo $this->theme->public_assets('images/services/service-v1-6.jpg');?>" alt="">
                        </div>
                        <div class="shape-bg" style="background-image: url(<?php echo $this->theme->public_assets('images/shape/service-image-shape.png', true);?>);"></div>
                    </div>
                    <div class="title-holder">
                        <h3><?php echo $this->lang->line('productsSectionCategoryWindowsAndStairs');?></h3>
                        <div class="inner-text">
                            <p><?php echo $this->lang->line('productsSectionCategoryWindowsAndStairsShortDescription');?></p>
                        </div>
                    </div>
                </div>
            </div>
            <!--End Single Service Style1-->
        </div>
    </div>
</section>
<!--End Service Style2 Area-->

<!--Start Working process area -->
<section class="working-process-area">
    <div class="shape1 wow slideInUp" data-wow-delay="100ms" data-wow-duration="5000ms" style="background-image: url(<?php echo $this->theme->public_assets('images/shape/working-process-shape-1.png');?>);"></div>
    <div class="shape2 wow slideInLeft" data-wow-delay="100ms" data-wow-duration="3500ms">
        <img class="float-bob-y" src="<?php echo $this->theme->public_assets('images/shape/working-process-shape-2.png');?>" alt="">
    </div>
    <div class="shape3 wow slideInDown" data-wow-delay="100ms" data-wow-duration="4500ms">
        <img class="float-bob-x" src="<?php echo $this->theme->public_assets('images/shape/working-process-shape-3.png');?>" alt="">
    </div>
    <div class="shape4 wow slideInRight" data-wow-delay="100ms" data-wow-duration="3500ms">
        <img class="zoom-fade" src="<?php echo $this->theme->public_assets('images/shape/working-process-shape-4.png');?>" alt="">
    </div>
    <div class="container">
        <div class="sec-title text-center">
            <div class="sub-title">
                <p><?php echo $this->lang->line('workingProcessSectionTitle');?></p>
            </div>
            <h2><?php echo $this->lang->line('workingProcessSectionSubTitle');?></h2>
        </div>
        <div class="row">
            <div class="col-xl-12">
                <ul class="working-process-box">
                    <!--Start Working process Single-->
                    <li class="single-working-process wow fadeInUp" data-wow-delay="00ms" data-wow-duration="1500ms">
                        <div class="arrow-box"><img src="<?php echo $this->theme->public_assets('images/shape/arrow.png');?>" alt=""></div>
                        <div class="img-holder">
                            <img src="<?php echo $this->theme->public_assets('images/resources/working-process-1.png');?>" alt="">
                            <div class="count"></div>
                        </div>
                        <div class="text-holder">
                            <h3><?php echo $this->lang->line('workingProcessSectionStepOneTitle');?></h3>
                            <p><?php echo $this->lang->line('workingProcessSectionStepOneSubTitle');?></p>
                        </div>
                    </li>
                    <!--End Working process Single-->
                    <!--Start Working process Single-->
                    <li class="single-working-process wow fadeInUp" data-wow-delay="100ms" data-wow-duration="1500ms">
                        <div class="arrow-box"><img src="<?php echo $this->theme->public_assets('images/shape/arrow-2.png');?>" alt=""></div>
                        <div class="img-holder">
                            <img src="<?php echo $this->theme->public_assets('images/resources/working-process-2.png');?>" alt="">
                            <div class="count"></div>
                        </div>
                        <div class="text-holder">
                            <h3><?php echo $this->lang->line('workingProcessSectionStepTwoTitle');?></h3>
                            <p><?php echo $this->lang->line('workingProcessSectionStepTwoSubTitle');?></p>
                        </div>
                    </li>
                    <!--End Working process Single-->
                    <!--Start Working process Single-->
                    <li class="single-working-process wow fadeInUp" data-wow-delay="200ms" data-wow-duration="1500ms">
                        <div class="img-holder">
                            <img src="<?php echo $this->theme->public_assets('images/resources/working-process-3.png');?>" alt="">
                            <div class="count"></div>
                        </div>
                        <div class="text-holder">
                            <h3><?php echo $this->lang->line('workingProcessSectionStepThreeTitle');?></h3>
                            <p><?php echo $this->lang->line('workingProcessSectionStepThreeSubTitle');?></p>
                        </div>
                    </li>
                    <!--End Working process Single-->
                </ul>
            </div>
            <div class="col-12 text-center">
                <a class="btn-one btn-one-gray scroll-to-target" data-target="#js-order-callback-section" data-offset="250" href="<?php echo site_url();?>#js-order-callback-section">
                    <span class="txt"><?php echo $this->lang->line('contactUsButtonText');?></span>
                </a>
            </div>
        </div>
    </div>
</section>
<!--End Working process area -->

<!--Start Project Style2 Area-->
<section class="project-style2-area" id="js-portfolio-section">
    <div class="container">
        <div class="sec-title text-center">
            <div class="sub-title">
                <p><?php echo $this->lang->line('ourProjectsSectionTitle');?></p>
            </div>
            <h2><?php echo $this->lang->line('ourProjectsSectionSubTitle');?></h2>
        </div>
        <div class="row masonary-layout">
            <!--Start Single project Item-->
            <div class="col-xl-4 col-lg-6 col-md-6">
                <div class="single-project-item">
                    <div class="img-holder">
                        <a class="lightbox-image" data-fancybox="gallery-projects" href="<?php echo $this->theme->public_assets('images/portfolio/project-v4-1.jpg');?>">
                            <img src="<?php echo $this->theme->public_assets('images/portfolio/project-v4-1-s.jpg');?>" alt="Крыша из металлочерепицы">
                        </a>
                    </div>
                </div>
            </div>
            <!--End Single project Item-->
            <!--Start Single project Item-->
            <div class="col-xl-4 col-lg-6 col-md-6">
                <div class="single-project-item">
                    <div class="img-holder">
                        <a class="lightbox-image" data-fancybox="gallery-projects" href="<?php echo $this->theme->public_assets('images/portfolio/project-v4-2.jpg');?>">
                            <img src="<?php echo $this->theme->public_assets('images/portfolio/project-v4-2-s.jpg');?>" alt="Крыша из металлочерепицы">
                        </a>
                    </div>
                </div>
            </div>
            <!--End Single project Item-->
            <!--Start Single project Item-->
            <div class="col-xl-4 col-lg-6 col-md-6">
                <div class="single-project-item">
                    <div class="img-holder">
                        <a class="lightbox-image" data-fancybox="gallery-projects" href="<?php echo $this->theme->public_assets('images/portfolio/project-v4-3.jpg');?>">
                            <img src="<?php echo $this->theme->public_assets('images/portfolio/project-v4-3-s.jpg');?>" alt="Крыша из металлочерепицы">
                        </a>
                    </div>
                </div>
            </div>
            <!--End Single project Item-->

            <!--Start Single project Item-->
            <div class="col-xl-4 col-lg-6 col-md-6">
                <div class="single-project-item">
                    <div class="img-holder">
                        <a class="lightbox-image" data-fancybox="gallery-projects" href="<?php echo $this->theme->public_assets('images/portfolio/project-v4-4.jpg');?>">
                            <img src="<?php echo $this->theme->public_assets('images/portfolio/project-v4-4-s.jpg');?>" alt="Крыша из профнастила">
                        </a>
                    </div>
                </div>
            </div>
            <!--End Single project Item-->

            <!--Start Single project Item-->
            <div class="col-xl-4 col-lg-6 col-md-6">
                <div class="single-project-item">
                    <div class="img-holder">
                        <a class="lightbox-image" data-fancybox="gallery-projects" href="<?php echo $this->theme->public_assets('images/portfolio/project-v4-6.jpg');?>">
                            <img src="<?php echo $this->theme->public_assets('images/portfolio/project-v4-6-s.jpg');?>" alt="Забор из профнастила">
                        </a>
                    </div>
                </div>
            </div>
            <!--End Single project Item-->

            <!--Start Single project Item-->
            <div class="col-xl-4 col-lg-6 col-md-6">
                <div class="single-project-item">
                    <div class="img-holder">
                        <a class="lightbox-image" data-fancybox="gallery-projects" href="<?php echo $this->theme->public_assets('images/portfolio/project-v4-5.jpg');?>">
                            <img src="<?php echo $this->theme->public_assets('images/portfolio/project-v4-5-s.jpg');?>" alt="Крыша из профнастила">
                        </a>
                    </div>
                </div>
            </div>
            <!--End Single project Item-->
        </div>
    </div>
</section>
<!--End Project Style1 Area-->

<!--Start About Style2 Area-->
<section class="about-style2-area" id="js-about-section">
    <div class="shape3 wow slideInRight" data-wow-delay="100ms" data-wow-duration="5000ms">
        <img class="zoom-fade" src="<?php echo $this->theme->public_assets('images/shape/about-style2-shape-2.png');?>" alt="" />
    </div>
    <div class="container">
        <div class="row">
            <div class="d-none d-lg-block col-lg-6">
                <div class="about-style2__image1">
                    <div class="red-box paroller"></div>
                    <div class="shape1 wow slideInRight" data-wow-delay="100ms" data-wow-duration="3500ms">
                        <img class="rotate-me-2" src="<?php echo $this->theme->public_assets('images/shape/about-style2-shape.png');?>" alt="" />
                    </div>
                    <div class="shape2 wow slideInLeft" data-wow-delay="100ms" data-wow-duration="5000ms">
                        <img class="rotate-me" src="<?php echo $this->theme->public_assets('images/shape/about-style2-shape.png');?>" alt="" />
                    </div>
                    <div class="main-image">
                        <img src="<?php echo $this->theme->public_assets('images/about/about-style2__image1.jpg');?>" alt="">
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="about-style1__content about-style1__content--style2">
                    <div class="sec-title">
                        <div class="sub-title">
                            <p><?php echo $this->lang->line('whoWeAreSectionTitle');?></p>
                        </div>
                        <h2><?php echo $this->lang->line('whoWeAreSectionSubTitle');?></h2>
                    </div>
                    <div class="inner-content">
                        <p><?php echo $this->lang->line('whoWeAreSectionDescription');?></p>

                        <ul>
                            <li class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.1s">
                                <div class="icon">
                                    <span class="flaticon-checked"></span>
                                </div>
                                <div class="text">
                                    <h3><?php echo $this->lang->line('whoWeAreSectionPointOneTitle');?></h3>
                                    <p><?php echo $this->lang->line('whoWeAreSectionPointOneSubTitle');?></p>
                                </div>
                            </li>
                            <li class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
                                <div class="icon">
                                    <span class="flaticon-checked"></span>
                                </div>
                                <div class="text">
                                    <h3><?php echo $this->lang->line('whoWeAreSectionPointTwoTitle');?></h3>
                                    <p><?php echo $this->lang->line('whoWeAreSectionPointTwoSubTitle');?></p>
                                </div>
                            </li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
<!--End About Style2 Area-->

<!--Start Fact Counter Style2 Area-->
<section class="fact-counter-style2-area" id="js-achievements-section">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="fact-counter_box fact-counter_box--style2">
                    <div class="pattern-bg" style="background-image: url(<?php echo $this->theme->public_assets('images/pattern/thm-pattern-1.png', true);?>);"></div>
                    <div class="sec-title text-center">
                        <div class="sub-title">
                            <p>Наши успехи за последние 5 лет</p>
                        </div>
                    </div>
                    <ul class="clearfix">

                        <!--Start Single Fact Counter-->
                        <li class="single-fact-counter wow slideInUp" data-wow-delay="00ms" data-wow-duration="1200ms">
                            <div class="icon">
                                <span class="flaticon-complete"></span>
                            </div>
                            <div class="outer-box">
                                <div class="count-outer count-box">
                                    <span class="count-text" data-speed="1200" data-stop="40000">0</span>
                                </div>
                                <div class="title">
                                    <h6><?php echo $this->lang->line('countersSectionPointOne');?></h6>
                                </div>
                            </div>
                        </li>
                        <!--End Single Fact Counter-->

                        <!--Start Single Fact Counter-->
                        <li class="single-fact-counter wow slideInUp" data-wow-delay="100ms" data-wow-duration="1200ms">
                            <div class="icon">
                                <span class="flaticon-sharing"></span>
                            </div>
                            <div class="outer-box">
                                <div class="count-outer count-box">
                                    <span class="count-text" data-speed="1200" data-stop="10000">0</span>
                                </div>
                                <div class="title">
                                    <h6><?php echo $this->lang->line('countersSectionPointTwo');?></h6>
                                </div>
                            </div>
                        </li>
                        <!--End Single Fact Counter-->

                        <!--Start Single Fact Counter-->
                        <li class="single-fact-counter wow slideInUp" data-wow-delay="200ms" data-wow-duration="1200ms">
                            <div class="icon">
                                <span class="flaticon-like"></span>
                            </div>
                            <div class="outer-box">
                                <div class="count-outer count-box">
                                    <span class="count-text" data-speed="1200" data-stop="100000">0</span>
                                </div>
                                <div class="title">
                                    <h6><?php echo $this->lang->line('countersSectionPointThree');?></h6>
                                </div>
                            </div>
                        </li>
                        <!--End Single Fact Counter-->

                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!--End Fact Counter Style2 Area-->

<!--Start Project Style2 Area-->
<section class="certificate-style1-area" id="js-certificates-section">
    <div class="container">
        <div class="sec-title text-center">
            <div class="sub-title">
                <p><?php echo $this->lang->line('ourCertificatesSectionTitle');?></p>
            </div>
            <h2><?php echo $this->lang->line('ourCertificatesSectionSubTitle');?></h2>
        </div>
        <div class="row masonary-layout">
            <!--Start Single project Item-->
            <div class="col-xl-4 col-lg-6 col-md-6">
                <div class="single-project-item">
                    <div class="img-holder">
                        <a class="lightbox-image" data-fancybox="certificates-projects" href="<?php echo $this->theme->public_assets('images/certificates/certificate-v1-1.jpg');?>">
                            <img src="<?php echo $this->theme->public_assets('images/certificates/certificate-v1-1s.jpg');?>" alt="">
                        </a>
                    </div>
                </div>
            </div>
            <!--End Single project Item-->
            <!--Start Single project Item-->
            <div class="col-xl-4 col-lg-6 col-md-6">
                <div class="single-project-item">
                    <div class="img-holder">
                        <a class="lightbox-image" data-fancybox="certificates-projects" href="<?php echo $this->theme->public_assets('images/certificates/certificate-v1-2.jpg');?>">
                            <img src="<?php echo $this->theme->public_assets('images/certificates/certificate-v1-2s.jpg');?>" alt="">
                        </a>
                    </div>
                </div>
            </div>
            <!--End Single project Item-->
            <!--Start Single project Item-->
            <div class="col-xl-4 col-lg-6 col-md-6">
                <div class="single-project-item">
                    <div class="img-holder">
                        <a class="lightbox-image" data-fancybox="certificates-projects" href="<?php echo $this->theme->public_assets('images/certificates/certificate-v1-3.jpg');?>">
                            <img src="<?php echo $this->theme->public_assets('images/certificates/certificate-v1-3s.jpg');?>" alt="">
                        </a>
                    </div>
                </div>
            </div>
            <!--End Single project Item-->

            <!--Start Single project Item-->
            <div class="col-xl-4 col-lg-6 col-md-6">
                <div class="single-project-item">
                    <div class="img-holder">
                        <a class="lightbox-image" data-fancybox="certificates-projects" href="<?php echo $this->theme->public_assets('images/certificates/certificate-v1-4.jpg');?>">
                            <img src="<?php echo $this->theme->public_assets('images/certificates/certificate-v1-4s.jpg');?>" alt="">
                        </a>
                    </div>
                </div>
            </div>
            <!--End Single project Item-->

            <!--Start Single project Item-->
            <div class="col-xl-4 col-lg-6 col-md-6">
                <div class="single-project-item">
                    <div class="img-holder">
                        <a class="lightbox-image" data-fancybox="certificates-projects" href="<?php echo $this->theme->public_assets('images/certificates/certificate-v1-5.jpg');?>">
                            <img src="<?php echo $this->theme->public_assets('images/certificates/certificate-v1-5s.jpg');?>" alt="">
                        </a>
                    </div>
                </div>
            </div>
            <!--End Single project Item-->

            <!--Start Single project Item-->
            <div class="col-xl-4 col-lg-6 col-md-6">
                <div class="single-project-item">
                    <div class="img-holder">
                        <a class="lightbox-image" data-fancybox="certificates-projects" href="<?php echo $this->theme->public_assets('images/certificates/certificate-v1-6.jpg');?>">
                            <img src="<?php echo $this->theme->public_assets('images/certificates/certificate-v1-6s.jpg');?>" alt="">
                        </a>
                    </div>
                </div>
            </div>
            <!--End Single project Item-->

        </div>
    </div>
</section>
<!--End Project Style1 Area-->

<!--Start Contact Style1 Area-->
<section class="contact-style1-area" id="js-order-callback-section">
    <div class="shape1">
        <img class="paroller-2" src="<?php echo $this->theme->public_assets('images/shape/contact-style1-shape-1.png', true);?>" alt="">
    </div>
    <div class="shape2">
        <img class="zoom-fade" src="<?php echo $this->theme->public_assets('images/shape/contact-style1-shape-2.png', true);?>" alt="">
    </div>
    <div class="shape3 float-bob-y" style="background-image: url(<?php echo $this->theme->public_assets('images/shape/contact-style1-shape-3.png', true);?>);"></div>
    <div class="container">
        <div class="row">
            <div class="col-xl-4">
                <div class="contact-style1__image wow slideInLeft" data-wow-delay="100ms" data-wow-duration="2500ms">
                    <img src="<?php echo $this->theme->public_assets('images/resources/house-1.png', true);?>" alt="">
                </div>
            </div>
            <div class="col-xl-8">
                <div class="contact-form-box1">
                    <div class="top-title">
                        <h2><?php echo $this->lang->line('contactUsFormTitle');?></h2>
                    </div>
                    <form id="js-contact-form-1" name="contact_form" class="default-form1">
                        <div class="row">
                            <div class="col-xl-4">
                                <div class="input-box">
                                    <input type="text" name="name" value="" placeholder="<?php echo $this->lang->line('contactUsFormInputNamePlaceholder');?>">
                                </div>
                            </div>
                            <div class="col-xl-4">
                                <div class="input-box">
                                    <input type="text" name="phone" value="" placeholder="<?php echo $this->lang->line('contactUsFormInputPhonePlaceholder');?>">
                                </div>
                            </div>
                            <div class="col-xl-4">
                                <div class="button-box">
                                    <button class="btn-one style2" type="submit">
                                        <span class="txt"><?php echo $this->lang->line('contactUsFormSubmitButtonText');?></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<!--End Contact Style1 Area-->

<!--Start Locations Area-->
<section class="locations-section-area" id="js-locations-section">
    <div class="container">
        <div class="row">
            <div class="col-12 col-xl-6">
                <div class="section-area-1">
                    <div class="sec-title text-center">
                        <h2><?php echo $this->lang->line('branchesSectionTitle');?></h2>
                    </div>
                    <div class="row">
                        <?php $geoBranches = Modules::run('branches/getListI18n');?>
                        <?php foreach($geoBranches as $geoBranch){?>
                            <div class="col-12 col-sm-6 col-lg-4 col-xl-6">
                                <div class="location-wrapper">
                                    <div class="location-city-name"><?php echo $geoBranch['name'];?></div>
                                    <div class="location-address"><?php echo $geoBranch['address'];?></div>
                                    <div class="location-phone">
                                        <?php foreach($geoBranch['phones'] as $geoBranchPhone){?>
                                            <a href="tel:<?php echo $geoBranchPhone;?>"><?php echo $geoBranchPhone;?></a>
                                        <?php }?>
                                    </div>
                                </div>
                            </div>
                        <?php }?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-area-2">
        <div class="locations-geomap-container" id="js-geomap-container"></div>
    </div>
    <script>
        var markersData = <?php echo json_encode($geoBranches);?>;
    </script>
</section>
<!--End Locations Area-->
