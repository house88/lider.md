<!-- Module css -->
<link rel="stylesheet" href="<?php echo $this->theme->public_assets('css/module-css/banner-section.css', true);?>">
<link rel="stylesheet" href="<?php echo $this->theme->public_assets('css/module-css/about-section.css', true);?>">
<link rel="stylesheet" href="<?php echo $this->theme->public_assets('css/module-css/fact-counter-section.css', true);?>">
<link rel="stylesheet" href="<?php echo $this->theme->public_assets('css/module-css/contact-page.css', true);?>">
<link rel="stylesheet" href="<?php echo $this->theme->public_assets('css/module-css/team-section.css', true);?>">
<link rel="stylesheet" href="<?php echo $this->theme->public_assets('css/module-css/partner-section.css', true);?>">
<link rel="stylesheet" href="<?php echo $this->theme->public_assets('css/module-css/services-section.css', true);?>">
<link rel="stylesheet" href="<?php echo $this->theme->public_assets('css/module-css/location-section.css', true);?>">