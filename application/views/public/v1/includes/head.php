<meta charset="UTF-8">
<base href="<?php echo base_url();?>">
<!-- responsive meta -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- For IE -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<title><?php echo $appSettings['default_title'];?></title>
<meta name="keywords" content="<?php echo $appSettings['default_mk'];?>" />
<meta name="description" content="<?php echo $appSettings['default_md'];?>" />

<!-- Google Fonts -->
<link
    href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
    rel="stylesheet">
<link
    href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
    rel="stylesheet">

<link rel="stylesheet" href="<?php echo $this->theme->public_assets('css/animate.css', true);?>">
<link rel="stylesheet" href="<?php echo $this->theme->public_assets('css/aos.css', true);?>">
<link rel="stylesheet" href="<?php echo $this->theme->public_assets('css/bootstrap.min.css', true);?>">
<link rel="stylesheet" href="<?php echo $this->theme->public_assets('css/custom-animate.css', true);?>">
<link rel="stylesheet" href="<?php echo $this->theme->public_assets('css/fancybox.min.css', true);?>">
<link rel="stylesheet" href="<?php echo $this->theme->public_assets('css/flaticon.css', true);?>">
<link rel="stylesheet" href="<?php echo $this->theme->public_assets('css/font-awesome.min.css', true);?>">
<link rel="stylesheet" href="<?php echo $this->theme->public_assets('css/imp.css', true);?>">
<link rel="stylesheet" href="<?php echo $this->theme->public_assets('css/jquery-ui.css', true);?>">
<link rel="stylesheet" href="<?php echo $this->theme->public_assets('css/owl.css', true);?>">
<link rel="stylesheet" href="<?php echo $this->theme->public_assets('css/scrollbar.css', true);?>">
<link rel="stylesheet" href="<?php echo $this->theme->public_assets('css/icomoon.css', true);?>">
<link rel="stylesheet" href="<?php echo $this->theme->public_assets('css/jquery.bootstrap-touchspin.css', true);?>">
<link rel="stylesheet" href="<?php echo $this->theme->public_assets('js/plugins/toastr/toastr.min.css', true);?>">

<!-- Module css -->
<link rel="stylesheet" href="<?php echo $this->theme->public_assets('css/module-css/header-section.css', true);?>">
<?php if(!empty($pageCssScripts)){?>
	<?php $this->load->view($this->theme->public_view($pageCssScripts)); ?>
<?php }?>
<link rel="stylesheet" href="<?php echo $this->theme->public_assets('css/module-css/footer-section.css', true);?>">

<link rel="stylesheet" href="<?php echo $this->theme->public_assets('css/style.css', true);?>">
<link rel="stylesheet" href="<?php echo $this->theme->public_assets('css/responsive.css', true);?>">
<!-- Favicon -->
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo $this->theme->public_assets('images/favicon/apple-touch-icon.png', true);?>">
<link rel="icon" type="image/png" href="<?php echo $this->theme->public_assets('images/favicon/favicon-32x32.png', true);?>" sizes="32x32">
<link rel="icon" type="image/png" href="<?php echo $this->theme->public_assets('images/favicon/favicon-16x16.png', true);?>" sizes="16x16">
<link rel="manifest" href="<?php echo $this->theme->public_assets('images/favicon/site.webmanifest', true);?>">
<link rel="mask-icon" href="<?php echo $this->theme->public_assets('images/favicon/safari-pinned-tab.svg', true);?>" color="#5bbad5">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="theme-color" content="#ffffff">

<script>
    Object.defineProperty(window, '_baseUrl', { writable: false, value: "<?php echo base_url();?>" });
    Object.defineProperty(window, '_siteUrl', { writable: false, value: "<?php echo site_url($this->lang->getLanguageUri($this->lang->lang()));?>" });
    Object.defineProperty(window, '_baseLangUrl', { writable: false, value: "<?php echo site_url($this->lang->getLanguageUri($this->lang->lang()));?>/" });
</script>
