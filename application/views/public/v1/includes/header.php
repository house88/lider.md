<!-- Preloader -->
<!-- <div class="loader-wrap">
	<div class="preloader">
		<div class="preloader-close"><i class="fa fa-times"></i></div>
	</div>
	<div class="layer layer-one"><span class="overlay"></span></div>
	<div class="layer layer-two"><span class="overlay"></span></div>
	<div class="layer layer-three"><span class="overlay"></span></div>
</div> -->

<!-- Main header-->
<header class="main-header header-style-two">
	<!--Start Header-->
	<div class="header">
		<div class="auto-container">
			<div class="outer-box">
				<div class="header-left">
					<div class="logo logo--style2">
						<div class="bg-box" style="background-image: url(<?php echo $this->theme->public_assets('images/shape/logo-style2-bg.png');?>);"></div>
						<a href="<?php echo site_url();?>"><img src="<?php echo $this->theme->public_assets('images/resources/logo.svg');?>" alt="LIDER" title=""></a>
					</div>
				</div>

				<div class="header-middle">
					<div class="nav-outer style1 clearfix">
						<!--Mobile Navigation Toggler-->
						<div class="mobile-nav-toggler">
							<div class="inner">
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</div>
						</div>
						<!-- Main Menu -->
						<nav class="main-menu style1 navbar-expand-md navbar-light">
							<div class="collapse navbar-collapse show clearfix" id="navbarSupportedContent">
								<ul class="navigation clearfix">
									<li>
										<a href="<?php echo site_url();?>"><?php echo $this->lang->line('homePageLinkText');?></a>
									</li>
									<li>
										<a class="scroll-to-target" data-target="#js-products-section" data-offset="100" href="<?php echo site_url();?>#js-products-section"><?php echo $this->lang->line('productsPageLinkText');?></a>
									</li>
									<li class="dropdown">
										<a><?php echo $this->lang->line('aboutUsPageLinkText');?></a>
										<ul>
											<li><a class="scroll-to-target" data-target="#js-about-section" data-offset="100" href="<?php echo site_url();?>#js-about-section"><?php echo $this->lang->line('aboutUsWhoWeArePageLinkText');?></a></li>
											<li><a class="scroll-to-target" data-target="#js-achievements-section" data-offset="350" href="<?php echo site_url();?>#js-achievements-section"><?php echo $this->lang->line('aboutUsOurAchievementsPageLinkText');?></a></li>
											<li><a class="scroll-to-target" data-target="#js-portfolio-section" data-offset="100" href="<?php echo site_url();?>#js-portfolio-section"><?php echo $this->lang->line('aboutUsOurPortfolioPageLinkText');?></a></li>
											<li><a class="scroll-to-target" data-target="#js-certificates-section" data-offset="100" href="<?php echo site_url();?>#js-certificates-section"><?php echo $this->lang->line('aboutUsOurCertificatesPageLinkText');?></a></li>
										</ul>
									</li>
									<li><a class="scroll-to-target" data-target="#js-order-callback-section" data-offset="250" href="<?php echo site_url();?>#js-order-callback-section"><?php echo $this->lang->line('aboutUsContactsPageLinkText');?></a></li>
								</ul>
							</div>
						</nav>
						<!-- Main Menu End-->
					</div>
				</div>

				<div class="header-right">
					<div class="space-box1"></div>
					<nav class="language-switcher navbar-light">
						<div class="collapse navbar-collapse">
							<ul class="navigation clearfix">
								<li class="dropdown">
									<?php $siteLanguages = $this->lang->getLanguages();?>
									<span><?php echo $siteLanguages['active']['name'];?></span>
									<ul>
										<?php foreach($siteLanguages['other'] as $anotherLanguage){?>
											<li><a href="<?php echo site_url($this->lang->getLanguageUri($anotherLanguage['uri']));?>"><?php echo $anotherLanguage['name'];?></a></li>
										<?php }?>
									</ul>
								</li>
							</ul>
						</div>
					</nav>
				</div>

			</div>
		</div>
	</div>
	<!--End header-->

	<!--Sticky Header-->
	<div class="sticky-header">
		<div class="container">
			<div class="clearfix">
				<!--Logo-->
				<div class="logo float-left">
					<a href="<?php echo site_url();?>" class="img-responsive">
						<img src="<?php echo $this->theme->public_assets('images/resources/logo.svg');?>" alt="" title="">
					</a>
				</div>
				<!--Right Col-->
				<div class="right-col float-right">
					<!-- Main Menu -->
					<nav class="main-menu clearfix" id="js-sticky-main-menu">
						<div class="collapse navbar-collapse show clearfix" id="navbarSupportedContentSticky">
							<ul class="navigation clearfix">
								<li>
									<a href="<?php echo site_url();?>"><?php echo $this->lang->line('homePageLinkText');?></a>
								</li>
								<li>
									<a class="scroll-to-target" data-target="#js-products-section" data-offset="100" href="<?php echo site_url();?>#js-products-section"><?php echo $this->lang->line('productsPageLinkText');?></a>
								</li>
								<li class="dropdown">
									<a class="scroll-to-target" data-target="#js-about-section" data-offset="100" href="<?php echo site_url();?>#js-about-section"><?php echo $this->lang->line('aboutUsPageLinkText');?></a>
									<ul>
										<li><a class="scroll-to-target" data-target="#js-about-section" data-offset="100" href="<?php echo site_url();?>#js-about-section"><?php echo $this->lang->line('aboutUsWhoWeArePageLinkText');?></a></li>
										<li><a class="scroll-to-target" data-target="#js-achievements-section" data-offset="350" href="<?php echo site_url();?>#js-achievements-section"><?php echo $this->lang->line('aboutUsOurAchievementsPageLinkText');?></a></li>
										<li><a class="scroll-to-target" data-target="#js-portfolio-section" data-offset="100" href="<?php echo site_url();?>#js-portfolio-section"><?php echo $this->lang->line('aboutUsOurPortfolioPageLinkText');?></a></li>
										<li><a class="scroll-to-target" data-target="#js-certificates-section" data-offset="100" href="<?php echo site_url();?>#js-certificates-section"><?php echo $this->lang->line('aboutUsOurCertificatesPageLinkText');?></a></li>
									</ul>
								</li>
								<li><a class="scroll-to-target" data-target="#js-order-callback-section" data-offset="250" href="<?php echo site_url();?>#js-order-callback-section"><?php echo $this->lang->line('aboutUsContactsPageLinkText');?></a></li>
								<li class="dropdown">
									<?php $siteLanguages = $this->lang->getLanguages();?>
									<span><?php echo $siteLanguages['active']['shortName'];?></span>
									<ul class="dropdown-menu dropdown-menu-right dropdown-language">
										<?php foreach($siteLanguages['other'] as $anotherLanguage){?>
											<li><a href="<?php echo site_url($this->lang->getLanguageUri($anotherLanguage['uri']));?>"><?php echo $anotherLanguage['name'];?></a></li>
										<?php }?>
									</ul>
								</li>
							</ul>
						</div>
					</nav>
				</div>
			</div>
		</div>
	</div>
	<!--End Sticky Header-->

	<!-- Mobile Menu  -->
	<div class="mobile-menu">
		<div class="menu-backdrop"></div>
		<div class="close-btn"><span class="icon fa fa-times-circle"></span></div>
		<nav class="menu-box">
			<div class="nav-logo">
				<a href="<?php echo site_url();?>">
					<img src="<?php echo $this->theme->public_assets('images/resources/logo-white.svg');?>" alt="" title="">
				</a>
			</div>
			<div class="menu-outer">
				<div class="collapse navbar-collapse show clearfix" id="navbarSupportedContentMobile">
					<ul class="navigation clearfix">
						<li>
							<a href="<?php echo site_url();?>"><?php echo $this->lang->line('homePageLinkText');?></a>
						</li>
						<li>
							<a class="scroll-to-target" data-target="#js-products-section" data-offset="100" href="<?php echo site_url();?>#js-products-section"><?php echo $this->lang->line('productsPageLinkText');?></a>
						</li>
						<li class="dropdown">
							<a class="scroll-to-target" data-target="#js-about-section" data-offset="100" href="<?php echo site_url();?>#js-about-section"><?php echo $this->lang->line('aboutUsPageLinkText');?></a>
							<ul>
								<li><a class="scroll-to-target" data-target="#js-about-section" data-offset="100" href="<?php echo site_url();?>#js-about-section"><?php echo $this->lang->line('aboutUsWhoWeArePageLinkText');?></a></li>
								<li><a class="scroll-to-target" data-target="#js-achievements-section" data-offset="350" href="<?php echo site_url();?>#js-achievements-section"><?php echo $this->lang->line('aboutUsOurAchievementsPageLinkText');?></a></li>
								<li><a class="scroll-to-target" data-target="#js-portfolio-section" data-offset="100" href="<?php echo site_url();?>#js-portfolio-section"><?php echo $this->lang->line('aboutUsOurPortfolioPageLinkText');?></a></li>
								<li><a class="scroll-to-target" data-target="#js-certificates-section" data-offset="100" href="<?php echo site_url();?>#js-certificates-section"><?php echo $this->lang->line('aboutUsOurCertificatesPageLinkText');?></a></li>
							</ul>
						</li>
						<li><a class="scroll-to-target" data-target="#js-order-callback-section" data-offset="250" href="<?php echo site_url();?>#js-order-callback-section"><?php echo $this->lang->line('aboutUsContactsPageLinkText');?></a></li>
						<li class="dropdown">
							<?php $siteLanguages = $this->lang->getLanguages();?>
							<span><?php echo $siteLanguages['active']['name'];?></span>
							<ul>
								<?php foreach($siteLanguages['other'] as $anotherLanguage){?>
									<li><a href="<?php echo site_url($this->lang->getLanguageUri($anotherLanguage['uri']));?>"><?php echo $anotherLanguage['name'];?></a></li>
								<?php }?>
							</ul>
						</li>
					</ul>
				</div>
			</div>
			<!--Social Links-->
			<div class="social-links">
				<ul class="clearfix">
					<li><a href="#"><span class="fab fa fa-facebook-square"></span></a></li>
					<li><a href="#"><span class="fab fa fa-twitter-square"></span></a></li>
					<li><a href="#"><span class="fab fa fa-pinterest-square"></span></a></li>
					<li><a href="#"><span class="fab fa fa-google-plus-square"></span></a></li>
					<li><a href="#"><span class="fab fa fa-youtube-square"></span></a></li>
				</ul>
			</div>
		</nav>
	</div>
	<!-- End Mobile Menu -->
</header>
