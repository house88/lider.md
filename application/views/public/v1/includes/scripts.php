<script src="<?php echo $this->theme->public_assets('js/jquery.js', true);?>"></script>
<script src="<?php echo $this->theme->public_assets('js/appear.js', true);?>"></script>
<script src="<?php echo $this->theme->public_assets('js/bootstrap.bundle.min.js', true);?>"></script>
<script src="<?php echo $this->theme->public_assets('js/jquery.easing.min.js', true);?>"></script>
<script src="<?php echo $this->theme->public_assets('js/jquery.enllax.min.js', true);?>"></script>
<script src="<?php echo $this->theme->public_assets('js/scrollbar.js', true);?>"></script>
<script src="<?php echo $this->theme->public_assets('js/jquery.bootstrap-touchspin.js', true);?>"></script>
<script src="<?php echo $this->theme->public_assets('js/general.js', true);?>"></script>

<?php if(!empty($pageJsScripts)){?>
	<?php $this->load->view($this->theme->public_view($pageJsScripts)); ?>
<?php }?>

<div class="modal" id="orderCallbackModal" tabindex="-1">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Заявка принята</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p>Мы получили информацию. Скоро с вами свяжется наш специалист.</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
			</div>
		</div>
	</div>
</div>