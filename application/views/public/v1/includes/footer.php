<!--Start footer area -->
<footer class="footer-area gray-bg">
	<!--Start Footer-->
	<div class="footer">
		<div class="footer-bg" style="background-image: url(<?php echo $this->theme->public_assets('images/resources/footer-bg.png', true);?>);"></div>
		<div class="container">
			<div class="row text-right-rtl">

				<!--Start single footer widget-->
				<div class="col-xl-5 col-lg-6 col-md-6 col-sm-12 wow animated fadeInUp" data-wow-delay="0.1s">
					<div class="single-footer-widget marbtm50">
						<div class="our-company-info">
							<div class="footer-logo">
								<a href="<?php echo site_url();?>"><img src="<?php echo $this->theme->public_assets('images/resources/logo-white.svg', true);?>"
										alt="" /></a>
							</div>
							<div class="text-box">
								<p><?php echo $this->lang->line('footerSectionSubLogoText');?></p>
							</div>
							<div class="open-hours">
								<h4><?php echo $this->lang->line('footerSectionSchedulerTitle');?></h4>
								<ul>
									<li><?php echo $this->lang->line('footerSectionSchedulerMoFr');?></li>
									<li><?php echo $this->lang->line('footerSectionSchedulerSa');?></li>
									<li><?php echo $this->lang->line('footerSectionSchedulerSu');?></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<!--End single footer widget-->
				<!--Start single footer widget-->
				<div class="col-xl-5 col-lg-6 col-md-6 col-sm-12 wow animated fadeInUp" data-wow-delay="0.3s">
					<div class="single-footer-widget margin__top marbtm50">
						<div class="title">
							<h3><?php echo $this->lang->line('footerSectionContactsTitle');?></h3>
						</div>
						<div class="footer-widget-contact-info">
							<ul>
								<li>
									<div class="inner">
										<div class="icon mapmarker">
											<span class="flaticon-placeholder"></span>
										</div>
										<div class="text">
											<p><?php echo $appSettings['default_address'];?></p>
										</div>
									</div>
								</li>
								<li>
									<div class="inner">
										<div class="icon">
											<span class="flaticon-phone"></span>
										</div>
										<div class="text">
											<p><?php echo $this->lang->line('footerSectionContactsCallUs');?> <a href="tel:<?php echo formatPhoneNumberInternational($appSettings['default_phone'])['formated'];?>"><?php echo formatPhoneNumber($appSettings['default_phone'])['formated'];?></a></p>
										</div>
									</div>
								</li>
								<li>
									<div class="inner">
										<div class="icon">
											<span class="flaticon-envelope-1"></span>
										</div>
										<div class="text">
											<p><a href="mailto:<?php echo $appSettings['default_email'];?>"><?php echo $appSettings['default_email'];?></a></p>
										</div>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!--End single footer widget-->
			</div>
		</div>
	</div>
	<!--End Footer-->


	<div class="footer-bottom">
		<div class="container">
			<div class="bottom-inner">
				<div class="copyright">
					<p><?php echo $this->lang->line('footerSectionCopyrightPhraseOne');?> &copy;<a href="<?php echo site_url();?>"> <?php echo date('Y');?> LIDER.</a> <?php echo $this->lang->line('footerSectionCopyrightPhraseTwo');?></p>
				</div>
				<div class="footer-social-link">
					<ul class="clearfix">
						<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
						<li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>

</footer>
<!--End footer area-->

<button class="scroll-top scroll-to-target" data-target="html" type="button">
	<span class="icon-right-arrow-1"></span>
</button>
