<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view($this->theme->public_view('includes/head')); ?>
</head>

<body>

	<div class="boxed_wrapper ltr">

		<?php $this->load->view($this->theme->public_view('includes/header')); ?>		

		<?php $this->load->view($this->theme->public_view($mainContent)); ?>

		<?php $this->load->view($this->theme->public_view('includes/footer')); ?>
		
	</div>

	<?php $this->load->view($this->theme->public_view('includes/scripts')); ?>
</body>

</html>