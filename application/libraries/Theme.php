<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Theme
{
	private $ci;
	private $themesApanelConfigs;
	private $themesPublicConfigs;
	private $themePublic;
	private $themePrefference;

	public function __construct(){
		$this->ci =& get_instance();
		$this->themesApanelConfigs = $this->ci->config->item('apanel_themes');
		$this->themesPublicConfigs = $this->ci->config->item('public_themes');

		$this->themePrefference = (int) get_cookie('_site_theme');
		$this->themePublic = !empty($this->themesPublicConfigs['other'][$this->themePrefference]) ? $this->themesPublicConfigs['other'][$this->themePrefference] : $this->themesPublicConfigs['default'];
	}     
	
	function apanel_assets($path = null){
		$themeInfo = $this->themesApanelConfigs['default'];
		return !empty($path) ? $themeInfo['assets_path'] . $path : 'The path: ' . $path . ' does not exist.';
	} 
	
	function apanel_view($path = null){
		$themeInfo = $this->themesApanelConfigs['default'];
		return !empty($path) ? $themeInfo['views_path'] . $path : 'The path: ' . $path . ' does not exist.';
	} 
	
	function public_view($path = null){		
		return !empty($path) ? $this->themePublic['views_path'] . $path : 'The path: ' . $path . ' does not exist.';
	}    
	
	function public_assets($path = null, $force_hash = false){
		return !empty($path) ? file_modification_time($this->themePublic['assets_path'] . $path, $force_hash) : 'The path: ' . $path . ' does not exist.';
	}
	
	function publicAssetsFilePath($path = null){
		return $this->themePublic['assets_path'] . $path;
	}

	function isCurrentOnPublic($id_theme){
		return $this->themePrefference === (int) $id_theme || (int) $this->themesPublicConfigs['default']['id_theme'] === (int) $id_theme;
	}
}
