<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use libphonenumber\PhoneNumberUtil;
use libphonenumber\NumberParseException;

function formatPhoneNumber($phone){
	$phoneNumberUtil = PhoneNumberUtil::getInstance();
	$raw_number = trim($phone);
	$result = array(
		'is_valid' => true,
		'formated' => null
	);

	try {
		if(!$phoneNumberUtil->isViablePhoneNumber($raw_number)){
			$result['is_valid'] = false;
			$result['error'][] = 'Phone number is not viable.';
		}

		$phone_number = $phoneNumberUtil->parse($raw_number, 'MD');
		if(!$phoneNumberUtil->isValidNumber($phone_number) && !$phoneNumberUtil->isValidNumberForRegion($phone_number, 'MD')){
			$result['is_valid'] = false;
			$result['error'][] = 'Phone number is not valid.1';
		}
	} catch (NumberParseException $exception) {
		$result['is_valid'] = false;
		$result['error'][] = 'Phone number is not valid.2';
	}

	if(false === $result['is_valid']){
		return $result;
	}

	$phoneNumberObject = $phoneNumberUtil->parse($raw_number, 'MD');
	$result['formated'] = $phoneNumberUtil->format($phoneNumberObject, \libphonenumber\PhoneNumberFormat::NATIONAL);

	return $result;
}

function formatPhoneNumberInternational($phone){
	$phoneNumberUtil = PhoneNumberUtil::getInstance();
	$raw_number = trim($phone);
	$result = array(
		'is_valid' => true,
		'formated' => null
	);

	try {
		if(!$phoneNumberUtil->isViablePhoneNumber($raw_number)){
			$result['is_valid'] = false;
			$result['error'][] = 'Phone number is not viable.';
		}

		$phone_number = $phoneNumberUtil->parse($raw_number, 'MD');
		if(!$phoneNumberUtil->isValidNumber($phone_number) && !$phoneNumberUtil->isValidNumberForRegion($phone_number, 'MD')){
			$result['is_valid'] = false;
			$result['error'][] = 'Phone number is not valid.1';
		}
	} catch (NumberParseException $exception) {
		$result['is_valid'] = false;
		$result['error'][] = 'Phone number is not valid.2';
	}

	if(false === $result['is_valid']){
		return $result;
	}

	$phoneNumberObject = $phoneNumberUtil->parse($raw_number, 'MD');
	$result['formated'] = $phoneNumberUtil->format($phoneNumberObject, \libphonenumber\PhoneNumberFormat::INTERNATIONAL);

	return $result;
}

function get_choice($value1 = null, $condition, $value2 = null)
{
	if (true === $condition) {
		return $value1;
	}

	if (false === $condition) {
		return $value2;
	}
}

function file_modification_time($link, $forced = false){
	$version = md5(filemtime( $link ));
	if($forced === true){
		$version = md5(uniqid());
	}

    return base_url($link).'?v='.$version;
}

function cleanOutput($str = ''){
	return htmlspecialchars($str, ENT_QUOTES, 'UTF-8', false);
}

function get_embed_video_link($link){
    $components = explode('=', $link);
    $video_id = $components[1];
    return 'https://youtube.com/embed/'.$video_id;
}

function getImage($img_path, $template_img = 'theme/images/no_image.png'){

	$path_parts = pathinfo($img_path);
	if(!empty($path_parts['extension']) && file_exists($img_path)){
		return $img_path;
	}else{
		return $template_img;
	}
}

function capitalWord($string, $delimiter_on = '_', $delimiter_off = ' '){
	$filtered = array();
	$conjunctions = array('of','the','and','or','not','but','yet','so','nor','as','for');
	$words = explode($delimiter_on, strtolower($string));

	foreach($words as $word)
		if(!in_array($word, $conjunctions)){
			$filtered[] = ucfirst($word);
		}else{
			$filtered[] = $word;
		}
	$filtered = array_filter($filtered);
	return implode($delimiter_off, $filtered);
}

function get_words($sentence, $count = 10) {
	preg_match("/(?:\w+(?:\W+|$)){0,$count}/", $sentence, $matches);
	return $matches[0];
}

function round_up($value, $precision) {
    $value = (float)$value;
	$precision = (int)$precision;

    if ($precision < 0) {
        $precision = 0;
	}

    $decPointPosition = strpos($value, '.');
    if ($decPointPosition === false) {
        return $value;
	}

    $floorValue = (float)substr($value, 0, $decPointPosition + $precision + 1);
	$followingDecimals = (int)substr($value, $decPointPosition + $precision + 1);

    if ($followingDecimals) {
        $ceilValue = $floorValue + pow(10, -$precision); // does this give always right result?
    } else{
        $ceilValue = $floorValue;
	}

	return $ceilValue;
}

/**
 * Returns an array with a column as a key grouped or not.
 *
 * @param array  $records    - array to change
 * @param string $key        - the name of the column that will become key
 * @param bool   $groupByKey - if true then all values are set as array in one key, if false then array column functionality used
 *
 * @return array
 */
function arrayByKey($records, $key, $groupByKey = false)
{
    $result = [];

    if (empty($records) || !is_array($records) || empty($key)) {
        return $result;
    }

    if (!$groupByKey) {
        return array_column($records, null, $key);
    }

    foreach ($records as $record) {
        if (!isset($record[$key])) {
            continue;
        }

        $result[$record[$key]][] = $record;
    }

    return $result;
}

function generate_video_html($id, $type, $w, $h, $autoplay = 0){
	if(empty($id)) return false;

	switch($type) {
		case 'vimeo':
			return '<iframe class="player bd-none" src="//player.vimeo.com/video/'.$id.'?autoplay='.$autoplay.'&title=0&amp;byline=0&amp;portrait=0&amp;color=13bdab" width="'.$w.'" height="'.$h.'" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
		break;
		case 'youtube':
			return  '<iframe class="player bd-none" width="'.$w.'" height="'.$h.'" src="//www.youtube.com/embed/'.$id.'?autoplay='.$autoplay.'" allowfullscreen></iframe>';
		break;
	}
}

function messageInModal($message, $type = 'errors'){
    $CI = &get_instance();
	$data['message'] = $message;
	$data['type'] = $type;
	echo $CI->load->view($CI->theme->public_view('includes/modal_systmess_view'), $data, true);
	exit();
}

function jsonResponse($message = '',$type = 'error', $additional_params = array()){
    $resp = $additional_params;
    $resp['mess_type'] = $type;
    $resp['mess_class'] = $type;
    $resp['message'] = $message;

    echo json_encode($resp);
    exit();
}

function jsonDTResponse($message = '',$additional_params = array(), $type = 'error'){
    $output = array(
		"iTotalRecords" => 0,
		"iTotalDisplayRecords" => 0,
		"aaData" => array()
    );

    $output = array_merge($output, $additional_params);
    $output['mess_type'] = $type;
    $output['message'] = $message;

    echo json_encode($output);
    exit();
}

function create_dir($dir, $mode = 0755){
    if(!is_dir($dir)){
        mkdir ($dir, $mode, true);
	}
}

function checkURI($uri_assoc = array(), $available_uri_segments = array(), $controller_data = array()){
	if(empty($uri_assoc) && empty($available_uri_segments)){
		return true;
	}

	foreach($uri_assoc as $key_uri => $value_uri){
		if(!in_array($key_uri, $available_uri_segments)){
			return_404($controller_data);
		}

		if(empty($value_uri)){
			return_404($controller_data);
		}
	}
}

function text_elipsis($str = "", $maxlength = 50){
	$trimmed_str = trim($str);
	if(mb_strlen($trimmed_str) > $maxlength){
		return mb_substr($trimmed_str, 0, $maxlength).'...';
	}

	return $trimmed_str;
}

// a function for comparing two float numbers
// float 1 - The first number
// float 2 - The number to compare against the first
// operator - The operator. Valid options are =, <=, <, >=, >, <>, eq, lt, lte, gt, gte, ne
function compare_float_numbers($float1, $float2, $operator="=")
{
	// Check numbers to 5 digits of precision
	$epsilon = 0.01;

	$float1 = (float)$float1;
	$float2 = (float)$float2;

	switch ($operator)
	{
		// equal
		case "=":
		case "eq":
		{
			if (abs($float1 - $float2) < $epsilon) {
				return true;
			}
			break;
		}
		// less than
		case "<":
		case "lt":
		{
			if (abs($float1 - $float2) < $epsilon) {
				return false;
			} else {
				if ($float1 < $float2) {
					return true;
				}
			}
			break;
		}
		// less than or equal
		case "<=":
		case "lte":
		{
			if (compare_float_numbers($float1, $float2, '<') || compare_float_numbers($float1, $float2, '=')) {
				return true;
			}
			break;
		}
		// greater than
		case ">":
		case "gt":
		{
			if (abs($float1 - $float2) < $epsilon) {
				return false;
			}
			else
			{
				if ($float1 > $float2) {
					return true;
				}
			}
			break;
		}
		// greater than or equal
		case ">=":
		case "gte":
		{
			if (compare_float_numbers($float1, $float2, '>') || compare_float_numbers($float1, $float2, '=')) {
				return true;
			}
			break;
		}
		case "<>":
		case "!=":
		case "ne":
		{
			if (abs($float1 - $float2) > $epsilon) {
				return true;
			}
			break;
		}
		default:
		{
			die("Unknown operator '".$operator."' in compareFloatNumbers()");
		}
	}

	return false;
}

function dt_ordering($source, array $map = array(), \Closure $transformer = null)
{
	$sorting_columns_amount = isset($source['iSortingCols']) ? (int) $source['iSortingCols'] : 0;
	if($sorting_columns_amount <= 0) {
		return array();
	}

	$ordering = array();
	for ($i = 0; $i < $sorting_columns_amount; $i++) {
		$column_direction_key = "sSortDir_{$i}";
		$column_index_key = "iSortCol_{$i}";
		if(
			!isset($source[$column_direction_key]) ||
			!isset($source[$column_index_key])
		) {
			continue;
		}

		$column_direction = mb_strtolower($source[$column_direction_key]);
		$column_index = (int) $source[$column_index_key];
		$column_key = "mDataProp_{$column_index}";
		if(
			!isset($source[$column_key]) ||
			!in_array($column_direction, array('asc', 'desc'), true)
		) {
			continue;
		}

		$column_alias = $source[$column_key];
		if(!isset($map[$column_alias])) {
			continue;
		}

		$ordering[] = array(
			'column'    => $map[$column_alias],
			'direction' => $column_direction,
		);
	}

	return $transformer instanceof \Closure ? array_map($transformer, $ordering) : $ordering;
}

function flat_dt_ordering($source, array $map = array())
{
	return dt_ordering($source, $map, function ($order) {
		return "{$order['column']}-{$order['direction']}";
	});
}

function validateDate($date, $format = "Y-m-d H:i:s"){
    $d = date_create_from_format($format, $date);
    return $d && date_format($d, $format) == $date;
}

function getDateFormat($date, $format = "Y-m-d H:i:s", $return_format = 'j M, Y H:i'){
    $d = date_create_from_format($format, $date);
	if($d && date_format($d, $format) == $date) {
    	return $d->format($return_format);
    }
}

function strForURL($str, $delimeter = '-', $lower = true){
	$rules = array(
		'Any-Latin;',
		'NFD;',
		'[:Nonspacing Mark:] Remove;',
		'NFC;',
		'[:Punctuation:] Remove;'
	);

	if($lower){
		$rules[] = 'Lower();';
	}

	$str = transliterator_transliterate(implode('', $rules), $str);
	$str = preg_replace('/[^a-zA-Z0-9\\/\_\ \-]/', '', $str);
    $str = preg_replace('/[-\s]+/', $delimeter, $str);
    return trim($str,$delimeter);
}

function translit($str, $delimeter = ' ', $lower = true){
	$rules = array(
		'Any-Latin;',
		'NFD;',
		'[:Nonspacing Mark:] Remove;',
		'NFC;',
		'[:Punctuation:] Remove;'
	);

	if($lower){
		$rules[] = 'Lower();';
	}

	$str = transliterator_transliterate(implode('', $rules), $str);
	$str = preg_replace('/[^a-zA-Z0-9\\/_|+ -]/', '', $str);
    $str = preg_replace('/[-\s]+/', $delimeter, $str);
    return trim($str,$delimeter);
}

function getHash($str = null, $split_length = 8){
	$token = hash('sha512', $str);

	if ($split_length !== false) {
		$token = implode('-', str_split($token, $split_length));
	}

	return $token;
}

function uniqueId($prefix = '', $lenght = 13) {
    // uniqid gives 13 chars, but you could adjust it to your needs.
    if (function_exists("random_bytes")) {
        $bytes = random_bytes(ceil($lenght / 2));
    } elseif (function_exists("openssl_random_pseudo_bytes")) {
        $bytes = openssl_random_pseudo_bytes(ceil($lenght / 2));
    } else {
        throw new Exception("no cryptographically secure random function available");
    }
    return $prefix . substr(bin2hex($bytes), 0, $lenght);
}
