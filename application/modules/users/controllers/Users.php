<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Users extends MX_Controller{
	
	function __construct(){
		parent::__construct();
		$this->load->model('user_model');
		
        $this->data = [];
        $this->breadcrumbs = [];

        $this->data['flash_message'] = (! isset($this->data['flash_message'])) ? $this->session->flashdata('flash_message') : $this->data['flash_message'];
	}
	
	function index(){
		return_404($this->data);
	}
	
	function account(){
		if(!$this->lauth->logged_in()){
			redirect('signin');
		}

		$this->breadcrumbs[] = array(
			'title' => 'Личный кабинет',
			'link' => base_url('account')
		);

		$this->data['stitle'] = 'Личный кабинет';

		$this->data['breadcrumbs'] = $this->breadcrumbs;

		$this->data['main_content'] = 'user/dashboard_view';
		$this->load->view($this->theme->public_view('shop_view'), $this->data);
		
	}

	function account_settings(){
		if(!$this->lauth->logged_in()){
			redirect('signin');
		}
		
		$this->breadcrumbs[] = array(
			'title' => 'Личный кабинет',
			'link' => base_url('account')
		);

		$this->breadcrumbs[] = array(
			'title' => 'Личные данные',
			'link' => base_url('account/settings')
		);

		$this->data['stitle'] = 'Личные данные';

		$this->data['user'] = $this->lauth->user_data();
		$this->data['breadcrumbs'] = $this->breadcrumbs;

		$this->data['main_content'] = 'user/settings_view';
		$this->load->view($this->theme->public_view('shop_view'), $this->data);
	}

	function ajax_operations(){
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}

		$action = $this->uri->segment(3);
		switch ($action) {
			case 'update':
				if(!$this->lauth->logged_in()){
					jsonResponse('Вы не авторизированы.');
				}

				$user = $this->lauth->user_data();
				$config = array(
					array(
						'field' => 'name',
						'label' => 'Имя',
						'rules' => 'required|trim|xss_clean|max_length[50]',
					),
					array(
						'field' => 'phone',
						'label' => 'Телефон',
						'rules' => 'xss_clean|max_length[50]',
					),
					array(
						'field' => 'user_city',
						'label' => 'Регион',
						'rules' => 'xss_clean|is_natural_no_zero',
					),
					array(
						'field' => 'address',
						'label' => 'Адрес',
						'rules' => 'xss_clean|max_length[250]',
					)
				);

				if($user->user_email != $this->input->post('email')){
					$config[] = array(
						'field' => 'email',
						'label' => 'E-mail',
						'rules' => 'trim|required|valid_email|is_unique[users.user_email]|xss_clean',
					);
				} else{
					$config[] = array(
						'field' => 'email',
						'label' => 'E-mail',
						'rules' => 'trim|required|valid_email|xss_clean',
					);
				}

				$this->form_validation->set_rules($config);

				if($this->form_validation->run() === false){
					jsonResponse($this->form_validation->error_array());
				}

				$update = array(
					'user_login' => $this->input->post('name',true),
					'user_nicename' => $this->input->post('name',true),
					'user_email' => $this->input->post('email',true),
					'user_phone' => $this->input->post('phone',true),
					'user_phone_display' => NULL,
					'user_address' => $this->input->post('address',true)
				);

				if($this->input->post('phone')){
					$phoneFormatResult = formatPhoneNumber($this->input->post('phone'));
					if(true === $phoneFormatResult['is_valid']){
						$update['user_phone_display'] = $phoneFormatResult['formated'];
					}
				}

				$id_city = (int) $this->input->post('user_city');
				$_city = Modules::run('cities/_get_city', $id_city);	
				if(!empty($_city)){
					$update['user_city'] = $id_city;
					set_cookie('_at_city', $id_city, 2592000);
				}
				
				$this->auth_model->handler_update($user->id, $update);

				jsonResponse('Сохраненно.', 'success');
			break;
			case 'update_city':
				$user = $this->lauth->user_data();
				$config = array(
					array(
						'field' => 'id_city',
						'label' => 'Город',
						'rules' => 'required|xss_clean|is_natural',
					)
				);
				$this->form_validation->set_rules($config);
				if($this->form_validation->run() === false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_city = (int) $this->input->post('id_city');

				if($id_city > 0){
					$_city = Modules::run('cities/_get_city', $id_city);
					if(!empty($_city)){
						set_cookie('_at_city', $id_city, 2592000);
					}
				} else{
					delete_cookie('_at_city');
				}

				if($this->lauth->logged_in()){
					$update = array(
						'user_city' => $id_city
					);				
					$this->auth_model->handler_update($user->id, $update);
					$this->session->set_userdata('user_city', $id_city);
				}

				jsonResponse('Сохраненно.', 'success');
			break;
			case 'change_password':
				if(!$this->lauth->logged_in()){
					jsonResponse('Вы не авторизированы.');
				}

				$user = $this->lauth->user_data();
				$config = array(
					array(
						'field' => 'old_password',
						'label' => 'Нынешний пароль',
						'rules' => 'required|trim|xss_clean',
					),
					array(
						'field' => 'password',
						'label' => 'Новый пароль',
						'rules' => 'required|trim|xss_clean|min_length[6]',
					),
					array(
						'field' => 'confirm_password',
						'label' => 'Повторите новый пароль',
						'rules' => 'required|trim|xss_clean|matches[password]',
					)
				);

				$this->form_validation->set_rules($config);
				if($this->form_validation->run() === false){
					jsonResponse($this->form_validation->error_array());
				}

				$old_password = $this->lauth->hash_password($this->input->post('old_password', true));
				if($old_password != $user->user_pass){
					jsonResponse('Нынешний пароль верный.');
				}

				$update = array(
					'reset_key' => md5($user->user_email.uniqid().'@ccreset'),
					'user_pass' => $this->lauth->hash_password($this->input->post('password',true))
				);
				$this->auth_model->handler_update($user->id, $update);

				jsonResponse('Пароль был изменен.', 'success');
			break;
			default:
				jsonResponse('Данные не верны.');
			break;
		}
	}

	function _get($id_user, $params = []){
		return $this->user_model->handler_get((int) $id_user, $params);
	}

	function _get_all($params = array()){
		return $this->user_model->handler_get_all((array) $params);
	}

	function _getGroups($params = []){
		return $this->user_model->handler_get_groups_all((array) $params);
	}

	function _update($id_user, $params = array()){
		return $this->auth_model->handler_update($id_user, (array) $params);
	}
}
