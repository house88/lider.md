<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Admin extends MX_Controller{
	function __construct(){
		parent::__construct();
		
		if(!$this->lauth->is_admin()){
			if (!$this->input->is_ajax_request()) {
				redirect('signin');
			} else{
				jsonResponse('Ошибка! Пройдите авторизацию.');
			}
		}

		$this->data = [];

		$this->data['flash_message'] = (! isset($this->data['flash_message'])) ? $this->session->flashdata('flash_message') : $this->data['flash_message'];
	}
	
	function index(){
		redirect('admin/settings');
	}
}
