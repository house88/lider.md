<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Color_palette extends MX_Controller{
	function __construct(){
		parent::__construct();
        
        $this->load->model('Color_palette_model', 'color_palette');
	}

    function list(){
        return $this->color_palette->handlerGetAll();
    }
}