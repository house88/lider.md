<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller{
	function __construct(){
		parent::__construct();

		if(!$this->lauth->is_admin()){
			if (!$this->input->is_ajax_request()) {
				redirect('/');
			} else{
				jsonResponse('Ошибка! Пройдите авторизацию.');
			}
		}

		$this->view_module_path = 'modules/color_palette/';
		$this->active_menu = 'settings';
		$this->active_submenu = 'color_palette';

        $this->data = [];

        $this->load->model('Color_palette_model', 'color_palette');

        $this->data['flash_message'] = (! isset($this->data['flash_message'])) ? $this->session->flashdata('flash_message') : $this->data['flash_message'];
	}

	function index(){
        checkPermision('manage_settings');

		$this->data['page_header'] = 'Цветовая палитра';
		$this->data['main_content'] = $this->theme->apanel_view($this->view_module_path . 'list_view');
		$this->data['main_scripts'] = $this->theme->apanel_view($this->view_module_path . 'scripts_view');
		$this->load->view($this->theme->apanel_view('dashboard'), $this->data);
	}

	function popup(){
		if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
		}
		
		$option = $this->uri->segment(4);
		switch ($option) {
			case 'add':
				checkPermisionAjax('manage_settings');

				$content = $this->load->view($this->theme->apanel_view($this->view_module_path . 'form_view'), $this->data, true);
				jsonResponse('', 'success', array('content' => $content));
			break;
			case 'update':
				checkPermisionAjax('manage_settings');

                $idColor = (int) $this->uri->segment(5);
                $this->data['colorInfo'] = $this->color_palette->handlerGet($idColor);

				$content = $this->load->view($this->theme->apanel_view($this->view_module_path . 'form_view'), $this->data, true);
				jsonResponse('', 'success', array('content' => $content));
			break;
			default:
				jsonResponse('Данные не верны.');
			break;
		}
	}

	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(4);
		switch($option){
            case 'setUpdate':
                checkPermisionAjax('manage_settings');

                $this->form_validation->set_rules('name_ro', 'Название RO', 'required|xss_clean|max_length[50]');
				$this->form_validation->set_rules('name_ru', 'Название RU', 'required|xss_clean|max_length[50]');
				$this->form_validation->set_rules('name_en', 'Название EN', 'required|xss_clean|max_length[50]');
				$this->form_validation->set_rules('hex_code', 'Цвет', 'required|xss_clean|max_length[10]');

                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }
                
                $setData = [
                    'name_ro' => $this->input->post('name_ro'),
					'name_ru' => $this->input->post('name_ru'),
					'name_en' => $this->input->post('name_en'),
                    'hex_code' => $this->input->post('hex_code'),
                    'sample_image_matt' => $this->input->post('sample_matt') ? $this->input->post('sample_matt') : NULL,
                    'sample_image_gloss' => $this->input->post('sample_matt') ? $this->input->post('sample_matt') : NULL
                ];
				
				$idColor = (int) $this->input->post('idColor');
				$colorInfo = $this->color_palette->handlerGet($idColor);
                if(empty($colorInfo)){
                    $idTheme = $this->color_palette->handlerInsert($setData);                    
                } else{
                    $this->color_palette->handlerUpdate($idColor, $setData);
                }

				jsonResponse('Сохранено.', 'success');
            break;
			case 'delete':
                checkPermisionAjax('manage_settings');

                $this->form_validation->set_rules('id', 'ИД', 'required|xss_clean');
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$idColor = (int) $this->input->post('id');
				$colorInfo = $this->color_palette->handlerGet($idColor);
                if(empty($colorInfo)){
                    jsonResponse('Операция прошла успешно.', 'success');
				}

				$this->themes->handlerDelete($idColor);
                
				jsonResponse('Операция прошла успешно.', 'success');
            break;
            case 'list':
                checkPermisionAjaxDT('manage_settings');

				$params = array(
					'limit' => (int) $this->input->post('iDisplayLength', true),
					'start' => (int) $this->input->post('iDisplayStart', true)
                );
        
                $records = $this->color_palette->handlerGetAll($params);
                $recordsCount = $this->color_palette->handlerCount($params);

				$output = array(
					"sEcho" => (int) $this->input->post('sEcho'),
					"iTotalRecords" => count($records),
					"iTotalDisplayRecords" => $recordsCount,
					"aaData" => array_map(function($record){
                        return array(
							'dt_id'				=>  $record['id'],
                            'dt_title'			=>  $record['name_ru'],
                            'dt_color'			=>  '<span class="btn" style="background:'.$record['hex_code'].';">'.$record['hex_code'].'</span>',
							'dt_actions'		=> '<div class="dropdown">
														<a data-toggle="dropdown" href="#" aria-expanded="false">
															<i class="fas fa-ellipsis-h"></i>
														</a>
														<div class="dropdown-menu dropdown-menu-right shadow-lg bg-white">
															<a class="dropdown-item call-popup" data-popup="#general_popup_form" href="#" data-href="'.base_url('admin/color_palette/popup/update/' . $record['id']).'">
																<i class="fad fa-pencil"></i> Редактировать
															</a>
															<div class="dropdown-divider"></div>
															<a href="#" class="dropdown-item call-function" data-callback="delete_action" data-id="'.$record['id'].'">
																<i class="fad fa-trash"></i> Удалить
															</a>
														</div>
													</div>'
						);
					}, $records)
				);

				jsonResponse('', 'success', $output);
            break;
		}
	}
}
