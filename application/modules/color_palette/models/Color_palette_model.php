<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Color_palette_model extends CI_Model{
	var $colorPaletteTable = "color_palette";
	function __construct(){
		parent::__construct();
	}

	function handlerInsert($data = []){
		if(empty($data)){
			return;
		}

		$this->db->insert($this->colorPaletteTable, $data);
		return $this->db->insert_id();
	}

	function handlerUpdate(int $id, $data = []){
		if(empty($data)){
			return;
		}

		$this->db->where('id', $id);
		$this->db->update($this->colorPaletteTable, $data);
	}

	function handlerDelete(int $id){
		$this->db->where('id', $id);
		$this->db->delete($this->colorPaletteTable);
	}

	function handlerGet(int $id){
		$this->db->where('id', $id);
		return $this->db->get($this->colorPaletteTable)->row_array();
	}

	function handlerGetAll($params = []){
        $order_by = " id ASC ";

        extract($params);

		$this->db->order_by($order_by);

		if(isset($limit, $start)){
			$this->db->limit($limit, $start);
		}

		return $this->db->get($this->colorPaletteTable)->result_array();
	}

	function handlerCount($params = []){
        extract($params);

		return $this->db->count_all_results($this->colorPaletteTable);
	}
}
