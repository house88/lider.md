<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Email extends MX_Controller{
	function __construct(){
        parent::__construct();
        
        
		$this->data = [];
        
        $this->load->library('email');
        
        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'smtp.yandex.ru';
        $config['smtp_port'] = 465;
        $config['smtp_crypto'] = 'ssl';
        $config['smtp_user'] = 'noreply@atehno.md';
        $config['smtp_pass'] = 'ziweffqdmvmfnukw';
        $config['mailtype'] = 'html';
        $config['charset'] = 'utf-8';
        $config['newline'] = "\r\n";
        $this->email->initialize($config);
    }
    
    function send($params = array()){
        if(empty($params)){
            return;
        }

        $this->email->from('noreply@atehno.md', $this->data['settings']['default_title']['setting_value']);
        $this->email->to($params['to']);					
        $this->email->subject($params['subject']);
        $this->email->message($this->load->view('email_templates/email_tpl', $params['email_data'], true));
        $this->email->set_mailtype("html");
        $this->email->send();
        
        $this->email->clear();
    }
}
