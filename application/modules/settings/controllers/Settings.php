<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Settings extends MX_Controller{
	function __construct(){
		parent::__construct();

		$this->data = [];

		$this->load->model('Settings_model', 'settings');

		$this->data['flash_message'] = (! isset($this->data['flash_message'])) ? $this->session->flashdata('flash_message') : $this->data['flash_message'];
	}

	function index(){
		show_404();
	}

	function _getSettings(){
		return arrayByKey($this->settings->getAll(), 'setting_alias');
	}

	function _getLangSettings(string $lang){
        $appSettings = $this->config->item('appSettings');
        $records = [];
        foreach ($appSettings as $appSettingsKey => $appSetting) {
            $records[$appSettingsKey] = cleanOutput(@$appSetting[$lang]);
        }

		return $records;
	}
}
