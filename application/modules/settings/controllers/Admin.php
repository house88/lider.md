<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Admin extends MX_Controller{
	function __construct(){
		parent::__construct();
		
		if(!$this->lauth->is_admin()){
			if (!$this->input->is_ajax_request()) {
				redirect('signin');
			} else{
				jsonResponse('Ошибка! Пройдите авторизацию.');
			}
		}
		
		$this->view_module_path = 'modules/admin/';
		$this->active_menu = 'settings';
		$this->active_submenu = 'settings';

		$this->data = array();

		$this->data['flash_message'] = (! isset($this->data['flash_message'])) ? $this->session->flashdata('flash_message') : $this->data['flash_message'];
	}
	
	function index(){
		$this->data['page_header'] = 'Настройки';
		
		$this->data['main_content'] = $this->theme->apanel_view($this->view_module_path . 'settings_view');
		$this->data['main_scripts'] = $this->theme->apanel_view($this->view_module_path . 'settings_scripts_view');
		$this->load->view($this->theme->apanel_view('dashboard'), $this->data);
	}
	
	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(4);
		switch($option){
			case 'update':
				if(!$this->lauth->have_right('manage_settings')){
					jsonResponse('Ошибка: Нет прав!');
				}

                $this->form_validation->set_rules('settings[]', 'Настройки', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }
				
				$postSettings = $this->input->post('settings');
				$update = [];
				$siteSettings = [];
				foreach($postSettings as $key => $setting){
					$update[] = array(
						'setting_alias' => $key,
						'value_ro' => $setting['ro'],
						'value_ru' => $setting['ru'],
						'value_en' => $setting['en']
					);

					$siteSettings[$key]['ro'] = $setting['ro'];
					$siteSettings[$key]['ru'] = $setting['ru'];
					$siteSettings[$key]['en'] = $setting['en'];
				}
				
				$this->load->model('settings/Settings_model', 'settings');
				$this->settings->update($update);
				
				$configContent = '<?php defined(\'BASEPATH\') OR exit(\'No direct script access allowed\');' . PHP_EOL;
				$configContent .= '$config[\'appSettings\'] = '. var_export($siteSettings, true) . ';' . PHP_EOL;

				$pathToFile = APPPATH . "config/app_settings.php";
				$file = fopen($pathToFile, "w");
				fwrite($file, $configContent);
				fclose($file);

				jsonResponse('Сохранено.', 'success');
			break;
		}
	}
}
