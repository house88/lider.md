<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Settings_model extends CI_Model{
	
	var $settings = "settings";
	
	function __construct(){
		parent::__construct();
	}
	
	function update($data = array()){
		if(empty($data)){
			return;
		}
		
		$this->db->update_batch($this->settings, $data, 'setting_alias');
	}
	
	function getAll(){
		$this->db->order_by('id', 'asc');
		return $this->db->get($this->settings)->result_array();
	}
}
