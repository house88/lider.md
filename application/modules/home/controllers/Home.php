<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MX_Controller{

	function __construct(){
		parent::__construct();

		$this->data = [];
		$this->breadcrumbs = [];

        $this->data['appSettings'] = Modules::run('settings/_getLangSettings', $this->lang->lang());
        $this->data['flash_message'] = (! isset($this->data['flash_message'])) ? $this->session->flashdata('flash_message') : $this->data['flash_message'];
	}

	function index(){
		$this->data['mainContent'] = 'content/home/index_view';
		$this->data['pageCssScripts'] = 'content/home/css_view';
		$this->data['pageJsScripts'] = 'content/home/scripts_view';
		$this->load->view($this->theme->public_view('index_view'), $this->data);
	}
}
