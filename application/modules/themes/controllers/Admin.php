<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller{
	function __construct(){
		parent::__construct();

		if(!$this->lauth->is_admin()){
			if (!$this->input->is_ajax_request()) {
				redirect('/');
			} else{
				jsonResponse('Ошибка! Пройдите авторизацию.');
			}
		}

		$this->view_module_path = 'modules/themes/';
		$this->active_menu = 'settings';
		$this->active_submenu = 'themes';

        $this->data = [];

        $this->load->model('Themes_model', 'themes');

        $this->data['flash_message'] = (! isset($this->data['flash_message'])) ? $this->session->flashdata('flash_message') : $this->data['flash_message'];
	}

	function index(){
        checkPermision('manage_themes');

		$this->data['page_header'] = 'Шаблошы сайта';
		$this->data['main_content'] = $this->theme->apanel_view($this->view_module_path . 'list_view');
		$this->data['main_scripts'] = $this->theme->apanel_view($this->view_module_path . 'scripts_view');
		$this->data['dt_filter'] = $this->theme->apanel_view($this->view_module_path . 'filter_view');
		$this->load->view($this->theme->apanel_view('dashboard'), $this->data);
	}

	function popup(){
		if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
		}
		
		$option = $this->uri->segment(4);
		switch ($option) {
			case 'setUpdate':
				checkPermisionAjax('manage_themes');

                $idTheme = (int) $this->uri->segment(5);
                $this->data['themeInfo'] = $this->themes->handlerGet($idTheme);

				$content = $this->load->view($this->theme->apanel_view($this->view_module_path . 'form_view'), $this->data, true);
				jsonResponse('', 'success', array('content' => $content));
			break;
			default:
				jsonResponse('Данные не верны.');
			break;
		}
	}

	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(4);
		switch($option){
            case 'setUpdate':
                checkPermisionAjax('manage_themes');

                $this->form_validation->set_rules('theme_name', 'Название', 'required|xss_clean|max_length[50]');
				$this->form_validation->set_rules('views_path', 'Папка views', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('assets_path', 'Папка assets', 'required|xss_clean|max_length[250]');

                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }
                
                $setData = [
                    'theme_name' => $this->input->post('theme_name'),
                    'theme_views_path' => $this->input->post('views_path'),
                    'theme_assets_path' => $this->input->post('assets_path')
                ];
				
				$idTheme = (int) $this->input->post('idTheme');
				$themeInfo = $this->themes->handlerGet($idTheme);
                if(empty($themeInfo)){
                    $idTheme = $this->themes->handlerInsert($setData);                    
                } else{
                    $this->themes->handlerUpdate($idTheme, $setData);
                }

				jsonResponse('Сохранено.', 'success');
            break;
			case 'delete':
                checkPermisionAjax('manage_themes');

                $this->form_validation->set_rules('id_theme', 'ИД шаблона', 'required|xss_clean');
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$idTheme = (int) $this->input->post('id_theme');
				$themeInfo = $this->themes->handlerGet($idTheme);
				if(empty($themeInfo)){
                    jsonResponse('Операция прошла успешно.', 'success');
				}

				if(1 === (int) $themeInfo['is_default']){
					jsonResponse('Нельзя удалить базовый шаблон.', 'warning');
				}

				$this->themes->handlerDelete($idTheme);
                
				jsonResponse('Операция прошла успешно.', 'success');
            break;
			case 'update_configs':
				checkPermisionAjax('manage_themes');

				$apanelConfig = [
					'default' => [
						'name' => 'Adminlte v.3',
						'views_path' => 'apanel/adminlte3/',
						'assets_path' => 'theme/apanel/adminlte3/'
					]
				];
				
				$configContent = '<?php defined(\'BASEPATH\') OR exit(\'No direct script access allowed\');' . PHP_EOL;
				$configContent .= '$config[\'apanel_themes\'] = '. var_export($apanelConfig, true) . ';' . PHP_EOL;
                
				$themesRecords = $this->themes->handlerGetAll();
				$publicConfig = [];
				foreach ($themesRecords as $themeRecord) {
					if(1 === (int) $themeRecord['is_default']){
						$publicConfig['default'] = [
							'id_theme' => $themeRecord['id_theme'],
							'name' => $themeRecord['theme_name'],
							'views_path' => $themeRecord['theme_views_path'],
							'assets_path' => $themeRecord['theme_assets_path'],
							'is_active' => (int) $themeRecord['is_active'],
							'is_default' => (int) $themeRecord['is_default']
						];
					} else{
						$publicConfig['other'][$themeRecord['id_theme']] = [
							'id_theme' => $themeRecord['id_theme'],
							'name' => $themeRecord['theme_name'],
							'views_path' => $themeRecord['theme_views_path'],
							'assets_path' => $themeRecord['theme_assets_path'],
							'is_active' => (int) $themeRecord['is_active'],
							'is_default' => (int) $themeRecord['is_default']
						];
					}
				}
                
				$configContent .= '$config[\'public_themes\'] = '. var_export($publicConfig, true) . ';' . PHP_EOL;

				$pathToFile = APPPATH . "config/themes.php";
				$file = fopen($pathToFile, "w");
				fwrite($file, $configContent);
				fclose($file);

				jsonResponse('Сохранено.', 'success');
			break;
            case 'list':
                checkPermisionAjaxDT('manage_themes');

				$params = array(
					'limit' => (int) $this->input->post('iDisplayLength', true),
					'start' => (int) $this->input->post('iDisplayStart', true)
                );
        
                $records = $this->themes->handlerGetAll($params);
                $recordsCount = $this->themes->handlerCount($params);

				$output = array(
					"sEcho" => (int) $this->input->post('sEcho'),
					"iTotalRecords" => count($records),
					"iTotalDisplayRecords" => $recordsCount,
					"aaData" => array_map(function($record){
						$isDefaultButton = '<span class="custom-font-size-24 '. get_choice('fad fa-check-square text-success', (int) $record['is_default'] === 1, 'fad fa-minus-square text-danger') .'"></span>';
						$isActiveButton = '<span class="custom-font-size-24 '. get_choice('fad fa-check-square text-success', (int) $record['is_active'] === 1, 'fad fa-minus-square text-danger') .'"></span>';
						
						$deleteButton = '';
						if(0 === (int) $record['is_default']){
							$deleteButton = '<div class="dropdown-divider"></div>
											<a href="#" class="dropdown-item call-function" data-callback="delete_action" data-theme="'.$record['id_theme'].'">
												<i class="fad fa-trash"></i> Удалить
											</a>';
						}

                        return array(
							'dt_id'				=>  $record['id_theme'],
                            'dt_title'			=>  $record['theme_name'],
                            'dt_views_path'		=>  $record['theme_views_path'],
                            'dt_assets_path'	=>  $record['theme_assets_path'],
							'dt_default'		=>  $isDefaultButton,
							'dt_active'			=>  $isActiveButton,
							'dt_actions'		=> '<div class="dropdown">
														<a data-toggle="dropdown" href="#" aria-expanded="false">
															<i class="fas fa-ellipsis-h"></i>
														</a>
														<div class="dropdown-menu dropdown-menu-right shadow-lg bg-white">
															<a class="dropdown-item call-popup" data-popup="#general_popup_form" href="#" data-href="'.base_url('admin/themes/popup/setUpdate/' . $record['id_theme']).'">
																<i class="fad fa-pencil"></i> Редактировать
															</a>
															'.$deleteButton.'
														</div>
													</div>'
						);
					}, $records)
				);

				jsonResponse('', 'success', $output);
            break;
		}
	}

	function _getSettings(){
		$this->config->load('themes', TRUE);

		return $this->config->item('themes');
	}
}
