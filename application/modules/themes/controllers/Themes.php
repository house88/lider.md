<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Themes extends MX_Controller{
	function __construct(){
		parent::__construct();
        
        $this->load->model('Themes_model', 'themes');
        $this->config->load('themes', TRUE, TRUE);

        $this->themesConfigs = $this->config->item('themes');
	}

    function list(){
        return $this->themes->handlerGetAll();
    }
}