<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Themes_model extends CI_Model{
	var $themesTable = "themes";
	function __construct(){
		parent::__construct();
	}

	function handlerInsert($data = []){
		if(empty($data)){
			return;
		}

		$this->db->insert($this->themesTable, $data);
		return $this->db->insert_id();
	}

	function handlerUpdate($idTheme, $data = []){
		if(empty($data)){
			return;
		}

		$this->db->where('id_theme', $idTheme);
		$this->db->update($this->themesTable, $data);
	}

	function handlerDelete($idTheme){
		$this->db->where('id_theme', $idTheme);
		$this->db->delete($this->themesTable);
	}

	function handlerGet($idTheme){
		$this->db->where('id_theme', $idTheme);
		return $this->db->get($this->themesTable)->row_array();
	}

	function handlerGetAll($params = []){
        $order_by = " id_theme ASC ";

        extract($params);

		$this->db->order_by($order_by);

		if(isset($limit, $start)){
			$this->db->limit($limit, $start);
		}

		return $this->db->get($this->themesTable)->result_array();
	}

	function handlerCount($params = []){
        extract($params);

		return $this->db->count_all_results($this->themesTable);
	}
}
