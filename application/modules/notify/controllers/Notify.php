<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Notify extends MX_Controller{
	function __construct(){
		parent::__construct();

		$this->data = [];

        $this->load->library('email');
        $this->data['flash_message'] = (! isset($this->data['flash_message'])) ? $this->session->flashdata('flash_message') : $this->data['flash_message'];
	}

	function index(){
		return_404($this->data);
	}

	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment($this->lang->getUriSegment(3));
		switch($option){
			case 'callme':
				$this->form_validation->set_rules('name', $this->lang->line('contactUsFormInputNamePlaceholder'), 'required|xss_clean|max_length[50]');
				$this->form_validation->set_rules('phone', $this->lang->line('contactUsFormInputPhonePlaceholder'), 'required|xss_clean|max_length[12]');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
				}

				$phone_display = $this->input->post('phone');
				if($this->input->post('phone')){
					$phoneFormatResult = formatPhoneNumber($this->input->post('phone'));
					if(true === $phoneFormatResult['is_valid']){
						$phone_display = $phoneFormatResult['formated'];
					}
				}

				Modules::run('email/send', array(
					'to' => 'cravciucandy@gmail.com',
					'subject' => 'Клиент нуждается в консультации',
					'email_data' => array(
						'user_name' => $this->input->post('name', true),
						'user_phone' => $phone_display,
						'title' => 'Клиент нуждается в консультации',
						'email_content' => 'callme_tpl'
					)
				));

				jsonResponse($this->lang->line('orderCallbackModalText'), 'success');
			break;
		}
	}

	function show_email(){
		$email_data = array(
            'user_name' => 'Andrei',
            'user_phone' => '684958673',
            'title' => 'Клиент нуждается в консультации',
            'email_content' => 'callme_tpl'
        );
		$this->load->view('email_templates/email_tpl',$email_data);
	}
}
