<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Branches_model extends CI_Model{
	
	var $mainTable = "branches";
	
	function __construct(){
		parent::__construct();
	}

	function handlerInsert($data = []){
		if(empty($data)){
			return;
		}

		$this->db->insert($this->mainTable, $data);
		return $this->db->insert_id();
	}

	function handlerInsertBatch($data = []){
		if(empty($data)){
			return;
		}

		$this->db->insert_batch($this->mainTable, $data);
		return $this->db->insert_id();
	}

	function handlerUpdate(int $id, $data = []){
		if(empty($data)){
			return;
		}

		$this->db->where('id', $id);
		$this->db->update($this->mainTable, $data);
	}

	function handlerDelete(int $id){
		$this->db->where('id', $id);
		$this->db->delete($this->mainTable);
	}

	function handlerGet(int $id){
		$this->db->where('id', $id);
		return $this->db->get($this->mainTable)->row_array();
	}

	function handlerGetAll($params = []){
        $order_by = " id ASC ";

        extract($params);

		$this->db->order_by($order_by);

		if(isset($limit, $start)){
			$this->db->limit($limit, $start);
		}

		return $this->db->get($this->mainTable)->result_array();
	}

	function handlerCount($params = []){
        extract($params);

		return $this->db->count_all_results($this->mainTable);
	}
}
