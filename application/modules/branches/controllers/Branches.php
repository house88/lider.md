<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Branches extends MX_Controller{
	function __construct(){
		parent::__construct();
		
		$this->load->model('Branches_model', 'branches');
	}

	function getListI18n(){
		$geoBranches = $this->branches->handlerGetAll();
		$records = [];
		foreach ($geoBranches as $geoBranch) {
			$markerContent = $this->load->view($this->theme->public_view('content/branches/map_marker_view'), ['geoBranch' => $geoBranch], true);
			$records[] = [
				"name" => $this->lang->getI18nColumn('city_name', $geoBranch), 
				"address" => $this->lang->getI18nColumn('address', $geoBranch),
				"phones" => json_decode($geoBranch['phones'], true), 
				"coordinates" => json_decode($geoBranch['coordinates'], true),
				"content" => $markerContent
			];
		}

		return $records;
	}

	function insert(){

		$insert = [
			[
				"city_name_ru" => "Кишинёв", 
				"city_name_ro" => "Chișinău", 
				"city_name_en" => "Chisinau", 
				"address_ru" => "ул. Петрикань, 31", 
				"address_ro" => "str. Petricani, 31", 
				"address_en" => "str. Petricani, 31", 
				"phones" => json_encode([
					"0 (22) 31 21 21", 
					"0 600 87 222" 
				]), 
				"coordinates" => json_encode([
					"latitude" => 47.055781, 
					"longitude" => 28.799483 
				])
			], 
			[
				"city_name_ru" => "Кагул", 
				"city_name_ro" => "Cahul", 
				"city_name_en" => "Cahul", 
				"address_ru" => "ул. Штефана чел Маре, 109/1", 
				"address_ro" => "str. Ștefan cel Mare, 109/1", 
				"address_en" => "str. Stefan cel Mare, 109/1", 
				"phones" => json_encode([
					"0 (299) 33 232", 
					"0 671 34 040" 
				]), 
				"coordinates" => json_encode([
					"latitude" => 45.91446, 
					"longitude" => 28.21139 
				])
			], 
			[
				"city_name_ru" => "Бельцы", 
				"city_name_ro" => "Bălți", 
				"city_name_en" => "Balti", 
				"address_ru" => "ул. Гагарина, 45", 
				"address_ro" => "str. Gagarina, 45", 
				"address_en" => "str. Gagarina, 45", 
				"phones" => json_encode([
					"0 (231) 28 390", 
					"0 691 19 215" 
				]), 
				"coordinates" => json_encode([
					"latitude" => 47.750033, 
					"longitude" => 27.944542 
				]) 
			], 
			[
				"city_name_ru" => "Комрат", 
				"city_name_ro" => "Comrat", 
				"city_name_en" => "Comrat", 
				"address_ru" => "ул. Ленина, 146", 
				"address_ro" => "str. Lenin, 146", 
				"address_en" => "str. Lenin, 146", 
				"phones" => json_encode([
					"0 (298) 84 544", 
					"0 696 51 600" 
				]), 
				"coordinates" => json_encode([
					"latitude" => 46.30748, 
					"longitude" => 28.65711 
				]) 
			], 
			[
				"city_name_ru" => "Оргеев", 
				"city_name_ro" => "Orhei", 
				"city_name_en" => "Orhei", 
				"address_ru" => "ул. 31 Августа 1989, 53", 
				"address_ro" => "str. 31 August 1989, 53", 
				"address_en" => "str. 31 August 1989, 53", 
				"phones" => json_encode([
					"0 (235) 84 244", 
					"0 671 05 198" 
				]), 
				"coordinates" => json_encode([
					"latitude" => 47.36044, 
					"longitude" => 28.81255 
				]) 
			], 
			[	
				"city_name_ru" => "Новые Анены", 
				"city_name_ro" => "Anenii Noi", 
				"city_name_en" => "Anenii Noi", 
				"address_ru" => "ул. Национального Примирения, 1", 
				"address_ro" => "str. Concelierii Naționale, 1", 
				"address_en" => "str. Concelierii Nationale, 1", 
				"phones" => json_encode([
					"0 (265) 21 202", 
					"0 790 22 727" 
				]), 
				"coordinates" => json_encode([
					"latitude" => 46.88313, 
					"longitude" => 29.234842 
				]) 
			], 
			[
				"city_name_ru" => "Бричаны", 
				"city_name_ro" => "Briceni", 
				"city_name_en" => "Briceni", 
				"address_ru" => "ул. Михай Еминеску, 17/Z", 
				"address_ro" => "str. Mihai Eminescu, 17/Z", 
				"address_en" => "str. Mihai Eminescu, 17/Z", 
				"phones" => json_encode([
					"0 (247) 22 090", 
					"0 600 01 535" 
				]), 
				"coordinates" => json_encode([
					"latitude" => 48.358016, 
					"longitude" => 27.0914 
				]) 
			], 
			[
				"city_name_ru" => "Яловены", 
				"city_name_ro" => "Ialoveni", 
				"city_name_en" => "Ialoveni", 
				"address_ru" => "Хынчештское шоссе, 252", 
				"address_ro" => "șos. Hâncești, 252", 
				"address_en" => "Hancesti Rd., 252", 
				"phones" => json_encode([
					"0 (22) 92 41 42", 
					"0 685 75 407" 
				]), 
				"coordinates" => json_encode([
					"latitude" => 46.975445, 
					"longitude" => 28.766876 
				]) 
			], 
			[
				"city_name_ru" => "Хынчешты", 
				"city_name_ro" => "Hâncești", 
				"city_name_en" => "Hancesti", 
				"address_ru" => "ул. Кишинёвская, 38", 
				"address_ro" => "str. Chișinăului, 38", 
				"address_en" => "str. Chisinaului, 38", 
				"phones" => json_encode([
					"0 (269) 81 911", 
					"0 686 79 449" 
				]), 
				"coordinates" => json_encode([
					"latitude" => 46.83319, 
					"longitude" => 28.60266 
				]) 
			], 
			[
				"city_name_ru" => "Унгены", 
				"city_name_ro" => "Ungheni", 
				"city_name_en" => "Ungheni", 
				"address_ru" => "ул. Влада Цепеша, 14", 
				"address_ro" => "str. Vlad Țepeș, 14", 
				"address_en" => "str. Vlad Tepes, 14", 
				"phones" => json_encode([
					"0 (236) 84 244", 
					"0 602 00 655" 
				]), 
				"coordinates" => json_encode([
					"latitude" => 47.20983, 
					"longitude" => 27.7946 
				]) 
			], 
			[
				"city_name_ru" => "Сороки", 
				"city_name_ro" => "Soroca", 
				"city_name_en" => "Soroca", 
				"address_ru" => "ул. Александру чел Бун, 53", 
				"address_ro" => "str. Alexandru cel Bun, 53", 
				"address_en" => "str. Alexandru cel Bun, 53", 
				"phones" => json_encode([
					"0 (230) 82 301", 
					"0 602 37 720" 
				]), 
				"coordinates" => json_encode([
					"latitude" => 48.14927, 
					"longitude" => 28.30058 
				]) 
			] 
		];

		// $this->branches->handlerInsertBatch($insert);
	}
}