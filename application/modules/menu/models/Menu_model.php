<?php
/*
Author: Daniel Gutierrez
Date: 9/18/12
Version: 1.0
*/

class Menu_model extends CI_Model{
    var $menu = "menu";

    function __construct(){
        parent::__construct();
    }

	function handler_apanel_menu(){
		return (object) array(
			'settings' => (object) array(
				'url' => '',
				'icon' => 'fad fa-cogs',
				'name' => 'Настройки',
				'rights' => 'manage_settings,manage_themes',
				'submenu' => (object) array(
					'settings' => (object) array(
						'url' => 'settings',
						'icon' => 'fad fa-sliders-v',
						'name' => 'Настройки',
						'rights' => 'manage_settings'
					),
					'color_palette' => (object) array(
						'url' => 'color_palette',
						'icon' => 'fad fa-palette',
						'name' => 'Цветовая палитра',
						'rights' => 'manage_settings'
					),
					'themes' => (object) array(
						'url' => 'themes',
						'icon' => 'fad fa-layer-group',
						'name' => 'Шаблоны сайта',
						'rights' => 'manage_themes'
					)
				)
			),
			'users' => (object) array(
				'url' => '',
				'icon' => 'fad fa-users-cog',
				'name' => 'Пользователи',
				'rights' => 'manage_users,manage_staff,manage_groups,manage_rights,manage_group_rights',
				'submenu' => (object) array(
					'users' => (object) array(
						'url' => 'users',
						'icon' => 'fad fa-users',
						'name' => 'Клиенты',
						'rights' => 'manage_users'
					),
					'staff' => (object) array(
						'url' => 'staff',
						'icon' => 'fad fa-user-shield',
						'name' => 'Администраторы',
						'rights' => 'manage_staff'
					),
					'groups' => (object) array(
						'url' => 'groups',
						'icon' => 'fad fa-users-class',
						'name' => 'Группы пользователей',
						'rights' => 'manage_groups'
					),
					'rights' => (object) array(
						'url' => 'rights',
						'icon' => 'fad fa-user-crown',
						'name' => 'Права пользователей',
						'rights' => 'manage_rights'
					),
					'group_rights' => (object) array(
						'url' => 'group_rights',
						'icon' => 'fad fa-users-crown',
						'name' => 'Права по группам',
						'rights' => 'manage_group_rights'
					)
				)
			)
		);
	}

	function handler_insert($data = array()){
        if(empty($data)){
            return;
        }

        $this->db->insert($this->menu, $data);
        return $this->db->insert_id();
	}

	function handler_update($id_menu, $data = array()){
        if(empty($data)){
            return;
        }

        $this->db->where('id_menu', $id_menu);
        $this->db->update($this->menu, $data);
	}

	function handler_delete($id_menu){
        $this->db->where('id_menu', $id_menu);
        $this->db->delete($this->menu);
	}

	function handler_get($id_menu){
        $this->db->where('id_menu', $id_menu);
        return $this->db->get($this->menu)->row_array();
	}

	function handler_get_by_title($menu_title){
        $this->db->where('menu_title', $menu_title);
        return $this->db->get($this->menu)->row_array();
	}

	function handler_get_all($conditions = array()){
        $order_by = " id_menu ASC ";

        extract($conditions);

		if (isset($sort_by)) {
			foreach ($sort_by as $sort_item) {
				$sort_item = explode('-', $sort_item);
				$multi_order_by[] = $sort_item[0] . ' ' . $sort_item[1];
			}
			$order_by = implode(',', $multi_order_by);
		}

        if(isset($active)){
			$this->db->where('menu_visible', $active);
        }

		$this->db->order_by($order_by);
		return $this->db->get($this->menu)->result_array();
	}

	function handler_get_count($conditions = array()){
        extract($conditions);

        if(isset($active)){
			$this->db->where('menu_visible', $active);
        }

		return $this->db->count_all_results($this->menu);
	}
}
