<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Auth extends MX_Controller{
	
	function __construct(){
		parent::__construct();
		
        $this->data = array();
        $this->breadcrumbs = array();

        $this->data['flash_message'] = (! isset($this->data['flash_message'])) ? $this->session->flashdata('flash_message') : $this->data['flash_message'];
	}
	
	function index(){
		return_404($this->data);
	}
	
	function signin(){
		if($this->lauth->logged_in()){
			if($this->lauth->is_admin()){
				redirect('admin');
			} else{
				redirect('/');
			}
		}

        $this->breadcrumbs[] = array(
			'title' => 'Авторизация',
			'link' => site_url('signin')
		);

		$this->load->view($this->theme->apanel_view('signin'), $this->data);

		// $this->data['breadcrumbs'] = $this->breadcrumbs;
		// $this->data['main_content'] = 'auth/signin_view';
		// $this->load->view($this->theme->public_view('shop_view'), $this->data);
	}

	function ajax_operations(){
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}

		$action = $this->uri->segment(4);
		switch ($action) {
			case 'signin':
				if($this->lauth->logged_in()){
					jsonResponse('Вы уже авторизированы.', 'info');
				}

				$this->form_validation->set_rules([
					[
						'field' => 'email',
						'label' => 'Email',
						'rules' => 'trim|required|xss_clean|valid_email',
					],
					[
						'field' => 'password',
						'label' => 'Password',
						'rules' => 'required|xss_clean',
					]
				]);

				if($this->form_validation->run() === false){
					jsonResponse($this->form_validation->error_array());
				}

				$authEmail = $this->input->post('email', true);
				$authPassword = $this->input->post('password', true);
				$userLoginInfo 	= $this->auth_model->handler_get_login_info($authEmail, $this->lauth->hash_password($authPassword));
				
				if($userLoginInfo){
					if($userLoginInfo->status == 0){
						jsonResponse('Учетная запись не активна.');
					}

					if($userLoginInfo->user_banned == 1){
						jsonResponse('Учетная запись заблокирована.');
					}

					$update = array(
						'signin_type' => 'site'
					);
					$this->auth_model->handler_update($userLoginInfo->id, $update);
					$userLoginInfo->signin_type = 'site';
					$this->lauth->set_sessiondata($userLoginInfo);					

					$_city = Modules::run('cities/_get_city', $userLoginInfo->user_city);	
					if(!empty($_city)){
						set_cookie('_at_city', $userLoginInfo->user_city, 2592000, '/');
					}

					if($this->lauth->is_admin()){
						$redirect = site_url('admin');
					} else{
						$redirect = site_url();
					}

                    jsonResponse('', 'success', array('redirect' => $redirect));
				} else{
					jsonResponse('Данные для входа неверны.');
				}
			break;
			default:
				jsonResponse('Данные не верны.' . $this->uri->total_segments());
			break;
		}
	}
	
	function logout(){
		$this->lauth->logout();
		redirect('');
	}
}
