<?php
$lang['homePageLinkText'] = 'Главная';
$lang['productsPageLinkText'] = 'Товары';
$lang['aboutUsPageLinkText'] = 'О нас';
$lang['aboutUsWhoWeArePageLinkText'] = 'Кто мы?';
$lang['aboutUsOurAchievementsPageLinkText'] = 'Наши достижения';
$lang['aboutUsOurPortfolioPageLinkText'] = 'Наши работы';
$lang['aboutUsOurCertificatesPageLinkText'] = 'Сертификаты';
$lang['aboutUsContactsPageLinkText'] = 'Контакты';
$lang['calculatorPageLinkText'] = 'Расчет кровли';
$lang['contactUsButtonText'] = 'Связаться с нами';
$lang['generalButtonViewDetailsText'] = 'Подробнее';
$lang['no_data'] = 'Нет данных для отображения.';

/**
 * Main slider section
 */
$lang['mainSliderTitle'] = 'Лидер<br> в производстве<br>металлочерепицы<br> и профнастила';
$lang['mainSliderSubTitleOne'] = 'Кровля, заборы, водосточные системы';
$lang['mainSliderSubTitleTwo'] = 'Широкий выбор цветов и текстур';
$lang['mainSliderSubTitleThree'] = 'Оперативность производства и доставки';
$lang['mainSliderSubTitleFour'] = 'Широкий ассортимент производимой продукции';
$lang['mainSliderGuaranty'] = 'Гарантия<br>до 30 лет';

/**
 * Products section
 */
$lang['productsSectionTitle'] = 'Наши товары';
$lang['productsSectionSubTitle'] = 'Мы предлагаем <br> самое лучшее качество';
$lang['productsSectionCategoryMetalTile'] = 'Металлочерепица';
$lang['productsSectionCategoryMetalTileShortDescription'] = 'Отличная сопротивляемость любым погодным условиям.';
$lang['productsSectionCategoryDrainageSystem'] = 'Водосточные системы';
$lang['productsSectionCategoryDrainageSystemShortDescription'] = 'Водосточные системы, подходящие для всех типов кровли.';
$lang['productsSectionCategoryFences'] = 'Ограждения';
$lang['productsSectionCategoryFencesShortDescription'] = 'Высококачественные штакетники и забор-жалюзи.';
$lang['productsSectionCategoryProfiledFlooring'] = 'Профнастил';
$lang['productsSectionCategoryProfiledFlooringShortDescription'] = 'Профнастил собственного производства для кровли и фасадов.';
$lang['productsSectionCategoryAdditionalElements'] = 'Доборные элементы';
$lang['productsSectionCategoryAdditionalElementsShortDescription'] = 'Мы предлагаем недорого купить доборные элементы для кровли.';
$lang['productsSectionCategoryWindowsAndStairs'] = 'Окна и Лестницы';
$lang['productsSectionCategoryWindowsAndStairsShortDescription'] = 'Мансардные окна и чердачные лестницы Fakro';

/**
 * Working process section
 */
$lang['workingProcessSectionTitle'] = 'Как мы работаем';
$lang['workingProcessSectionSubTitle'] = 'Мы работаем - Вы отдыхаете';
$lang['workingProcessSectionStepOneTitle'] = 'Бесплатный замер';
$lang['workingProcessSectionStepOneSubTitle'] = 'Наши специалисты сделают все нужные замеры и помогут с выбором материала.';
$lang['workingProcessSectionStepTwoTitle'] = 'Быстрое производство';
$lang['workingProcessSectionStepTwoSubTitle'] = 'Только лучшие материалы. В кратчайшие сроки для вашего уюта.';
$lang['workingProcessSectionStepThreeTitle'] = 'Оперативная доставка';
$lang['workingProcessSectionStepThreeSubTitle'] = 'Мы не заставим вас долго ждать, потому что мы знаем цену времени.';

/**
 * Our projects section
 */
$lang['ourProjectsSectionTitle'] = 'Наши работы';
$lang['ourProjectsSectionSubTitle'] = 'Ознакомьтесь с нашими<br> проектами';

/**
 * Who we are section
 */
$lang['whoWeAreSectionTitle'] = 'Кто мы?';
$lang['whoWeAreSectionSubTitle'] = 'Лидеры в производстве металлочерепицы и профнастила.';
$lang['whoWeAreSectionDescription'] = 'Основным приоритетом компании является оказание полного комплекса услуг, от бесплатного выезда на объект для замеров, до бесплатной доставки изготовленной продукции.';
$lang['whoWeAreSectionPointOneTitle'] = 'Сегодня LIDER';
$lang['whoWeAreSectionPointOneSubTitle'] = 'Одна из ведущих компаний на рынке кровельных материалов.';
$lang['whoWeAreSectionPointTwoTitle'] = 'Направление компаний';
$lang['whoWeAreSectionPointTwoSubTitle'] = 'Группа компаний LIDER является надёжным поставщиком кровельных материалов и доборных элементы от ведущих мировых производителей.';

/**
 * Counters section
 */
$lang['countersSectionPointOne'] = 'Жилых проектов';
$lang['countersSectionPointTwo'] = 'Коммерческих проектов';
$lang['countersSectionPointThree'] = 'Довольных клиентов';

/**
 * Our certificates section
 */
$lang['ourCertificatesSectionTitle'] = 'Наши сертификаты';
$lang['ourCertificatesSectionSubTitle'] = 'Ознакомьтесь с нашими<br> сертификатами';

/**
 * Contact Us section
 */
$lang['contactUsFormTitle'] = 'Нужна помощь<br> специалиста?';
$lang['contactUsFormInputNamePlaceholder'] = 'Имя';
$lang['contactUsFormInputPhonePlaceholder'] = 'Телефон';
$lang['contactUsFormSubmitButtonText'] = 'Позвоните мне';

/**
 * Branches section
 */
$lang['branchesSectionTitle'] = 'Наши филиалы';

/**
 * Footer section
 */
$lang['footerSectionSubLogoText'] = 'Производство и продажа материалов для строительных и отделочных работ.';
$lang['footerSectionSchedulerTitle'] = 'График работы:';
$lang['footerSectionSchedulerMoFr'] = 'Пн. - Пт. 09:00–18:00';
$lang['footerSectionSchedulerSa'] = 'Сб. 09:00–14:00';
$lang['footerSectionSchedulerSu'] = 'Вс. - выходной';
$lang['footerSectionContactsTitle'] = 'Адресс';
$lang['footerSectionContactsCallUs'] = 'Позвоните нам:';
$lang['footerSectionContactsPhone'] = '0(22) 312 121';
$lang['footerSectionContactsEmail'] = 'info@lider.md';
$lang['footerSectionCopyrightPhraseOne'] = 'Авторское право';
$lang['footerSectionCopyrightPhraseTwo'] = 'Все права защищены.';

/**
 * Order Call-back modal
 */
$lang['orderCallbackModalTitle'] = 'Заявка принята';
$lang['orderCallbackModalText'] = 'Мы получили информацию. Скоро с вами свяжется наш специалист.';
$lang['orderCallbackModalCloseButtonText'] = 'Закрыть';
