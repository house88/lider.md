<?php
$lang['homePageLinkText'] = 'Acasa';
$lang['productsPageLinkText'] = 'Produse';
$lang['aboutUsPageLinkText'] = 'Despre noi';
$lang['aboutUsWhoWeArePageLinkText'] = 'Cine suntem?';
$lang['aboutUsOurAchievementsPageLinkText'] = 'Realizările noastre';
$lang['aboutUsOurPortfolioPageLinkText'] = 'Lucrarile noastre';
$lang['aboutUsOurCertificatesPageLinkText'] = 'Certificate';
$lang['aboutUsContactsPageLinkText'] = 'Contacte';
$lang['calculatorPageLinkText'] = 'Calculul Acoperișului';
$lang['contactUsButtonText'] = 'Contactați-ne';
$lang['generalButtonViewDetailsText'] = 'Detalii';
$lang['no_data'] = 'Нет данных для отображения.';

/**
 * Main slider
 */
$lang['mainSliderTitle'] = 'Lider<br> în producția de<br>plăci metalice și<br>pardoseli profilate';
$lang['mainSliderSubTitleOne'] = 'Acoperișuri, garduri, sisteme de drenaj';
$lang['mainSliderSubTitleTwo'] = 'Gamă largă de culori și texturi';
$lang['mainSliderSubTitleThree'] = 'Promptitudinea producției și livrării';
$lang['mainSliderSubTitleFour'] = 'O gamă largă de produse fabricate';
$lang['mainSliderGuaranty'] = 'Garanție<br>până la 30 ani';

/**
 * Products section
 */
$lang['productsSectionTitle'] = 'Produsele noastre';
$lang['productsSectionSubTitle'] = 'Noi vă oferim<br>cea mai buna calitate';
$lang['productsSectionCategoryMetalTile'] = 'Țiglă metalică';
$lang['productsSectionCategoryMetalTileShortDescription'] = 'Rezistență excelentă la toate condițiile meteorologice.';
$lang['productsSectionCategoryDrainageSystem'] = 'Sisteme de drenaj';
$lang['productsSectionCategoryDrainageSystemShortDescription'] = 'Sisteme de drenaj potrivite pentru toate tipurile de acoperișuri.';
$lang['productsSectionCategoryFences'] = 'Garduri';
$lang['productsSectionCategoryFencesShortDescription'] = 'Garduri de înaltă calitate și jaluzele de gard.';
$lang['productsSectionCategoryProfiledFlooring'] = 'Podele profilate';
$lang['productsSectionCategoryProfiledFlooringShortDescription'] = 'Pardoseli profilate de producție proprie pentru acoperișuri și fațade.';
$lang['productsSectionCategoryAdditionalElements'] = 'Elemente suplimentare';
$lang['productsSectionCategoryAdditionalElementsShortDescription'] = 'Oferim preț avantajos la elemente suplimentare pentru acoperiș.';
$lang['productsSectionCategoryWindowsAndStairs'] = 'Ferestre și scări';
$lang['productsSectionCategoryWindowsAndStairsShortDescription'] = 'Ferestre de mansardă de înaltă calitate și scări de mansardă Fakro.';

/**
 * Working process section
 */
$lang['workingProcessSectionTitle'] = 'Procesul de lucru';
$lang['workingProcessSectionSubTitle'] = 'Noi lucrăm - Voi vă odihniți';
$lang['workingProcessSectionStepOneTitle'] = 'Măsurare gratuită';
$lang['workingProcessSectionStepOneSubTitle'] = 'Specialiștii noștri vor lua măsurile și vor ajuta la alegerea materialului.';
$lang['workingProcessSectionStepTwoTitle'] = 'Producție rapidă';
$lang['workingProcessSectionStepTwoSubTitle'] = 'Numai cele mai bune materiale. În cel mai scurt timp pentru confortul dvs.';
$lang['workingProcessSectionStepThreeTitle'] = 'Livrare promptă';
$lang['workingProcessSectionStepThreeSubTitle'] = 'Nu vă vom face să așteptați mult pentru că știm valoarea timpului.';

/**
 * Our projects section
 */
$lang['ourProjectsSectionTitle'] = 'Lucrările noastre';
$lang['ourProjectsSectionSubTitle'] = 'Vezi proiectele noastre';

/**
 * Who we are section
 */
$lang['whoWeAreSectionTitle'] = 'Cine suntem?';
$lang['whoWeAreSectionSubTitle'] = 'Lideri în producția de plăci metalice și pardoseli profilate.';
$lang['whoWeAreSectionDescription'] = 'Principala prioritate a companiei este de a oferi o gamă completă de servicii, de la plecarea gratuită la instalația de măsurare, până la livrarea gratuită a produselor fabricate.';
$lang['whoWeAreSectionPointOneTitle'] = 'Astăzi LIDER';
$lang['whoWeAreSectionPointOneSubTitle'] = 'Una dintre cele mai importante companii de pe piața materialelor de acoperiș.';
$lang['whoWeAreSectionPointTwoTitle'] = 'Direcția companiei';
$lang['whoWeAreSectionPointTwoSubTitle'] = 'Grupul de companii LIDER este un furnizor fiabil de materiale pentru acoperișuri și elemente de finisare de la cei mai importanți producători mondiali.';

/**
 * Counters section
 */
$lang['countersSectionPointOne'] = 'Proiecte rezidentiale';
$lang['countersSectionPointTwo'] = 'Proiecte comerciale';
$lang['countersSectionPointThree'] = 'Clienți satisfăcuți';

/**
 * Our certificates section
 */
$lang['ourCertificatesSectionTitle'] = 'Certificatele noastre';
$lang['ourCertificatesSectionSubTitle'] = 'Faceți cunoștință<br> cu certificatele noastre';

/**
 * Contact Us section
 */
$lang['contactUsFormTitle'] = 'Aveți nevoie de<br> ajutorul specialistului?';
$lang['contactUsFormInputNamePlaceholder'] = 'Numele';
$lang['contactUsFormInputPhonePlaceholder'] = 'Telefon';
$lang['contactUsFormSubmitButtonText'] = 'Rog sa mă sunați';

/**
 * Branches section
 */
$lang['branchesSectionTitle'] = 'Filialele noastre';

/**
 * Footer section
 */
$lang['footerSectionSubLogoText'] = 'Producția și vânzarea de materiale pentru construcții și lucrări de finisare.';
$lang['footerSectionSchedulerTitle'] = 'Program de lucru:';
$lang['footerSectionSchedulerMoFr'] = 'Lu. - Vi. 09:00–18:00';
$lang['footerSectionSchedulerSa'] = 'Sâ. 09:00–14:00';
$lang['footerSectionSchedulerSu'] = 'Dum. - zi liberă';
$lang['footerSectionContactsTitle'] = 'Adresa';
$lang['footerSectionContactsCallUs'] = 'Contactați-ne la tel.:';
$lang['footerSectionContactsPhone'] = '0(22) 312 121';
$lang['footerSectionContactsEmail'] = 'info@lider.md';
$lang['footerSectionCopyrightPhraseOne'] = 'Drepturile de autor';
$lang['footerSectionCopyrightPhraseTwo'] = 'Toate drepturile sunt rezervate.';

/**
 * Order Call-back modal
 */
$lang['orderCallbackModalTitle'] = 'Cererea a fost primită';
$lang['orderCallbackModalText'] = 'Noi am primit cererea. În curând un reprezentat al companiei va lua legatura cu dumneavoastră.';
$lang['orderCallbackModalCloseButtonText'] = 'Închide';
