<?php
$lang['homePageLinkText'] = 'Home';
$lang['productsPageLinkText'] = 'Products';
$lang['aboutUsPageLinkText'] = 'About us';
$lang['aboutUsWhoWeArePageLinkText'] = 'Who we are?';
$lang['aboutUsOurAchievementsPageLinkText'] = 'Our achievements';
$lang['aboutUsOurPortfolioPageLinkText'] = 'Porfolio';
$lang['aboutUsOurCertificatesPageLinkText'] = 'Certificates';
$lang['aboutUsContactsPageLinkText'] = 'Contacts';
$lang['calculatorPageLinkText'] = 'Roof Calculation';
$lang['contactUsButtonText'] = 'Contact us';
$lang['generalButtonViewDetailsText'] = 'More Details';
$lang['no_data'] = 'There are nothing to display.';

/**
 * Main slider section
 */
$lang['mainSliderTitle'] = 'The Leader<br> in the Production of<br> Metal Tiles and<br> Profiled flooring';
$lang['mainSliderSubTitleOne'] = 'Roofing, fences, drainage systems';
$lang['mainSliderSubTitleTwo'] = 'Wide range of colors and textures';
$lang['mainSliderSubTitleThree'] = 'Promptness of production and delivery';
$lang['mainSliderSubTitleFour'] = 'A wide range of manufactured products';
$lang['mainSliderGuaranty'] = 'Guaranty<br>up to 30 years';

/**
 * Products section
 */
$lang['productsSectionTitle'] = 'Our Products';
$lang['productsSectionSubTitle'] = 'We Offer<br>the Best Quality';
$lang['productsSectionCategoryMetalTile'] = 'Metal tile';
$lang['productsSectionCategoryMetalTileShortDescription'] = 'Excellent resistance to all weather conditions.';
$lang['productsSectionCategoryDrainageSystem'] = 'Drainage systems';
$lang['productsSectionCategoryDrainageSystemShortDescription'] = 'Drainage systems suitable for all types of roofing.';
$lang['productsSectionCategoryFences'] = 'Fences';
$lang['productsSectionCategoryFencesShortDescription'] = 'High-quality picket fences and fence blinds.';
$lang['productsSectionCategoryProfiledFlooring'] = 'Profiled flooring';
$lang['productsSectionCategoryProfiledFlooringShortDescription'] = 'Profiled flooring of own production for roofs and facades.';
$lang['productsSectionCategoryAdditionalElements'] = 'Additional elements';
$lang['productsSectionCategoryAdditionalElementsShortDescription'] = 'We offer cheap to buy additional elements for the roof.';
$lang['productsSectionCategoryWindowsAndStairs'] = 'Windows and Stairs';
$lang['productsSectionCategoryWindowsAndStairsShortDescription'] = 'High-quality attic windows and attic stairs Fakro.';

/**
 * Working process section
 */
$lang['workingProcessSectionTitle'] = 'How We Work';
$lang['workingProcessSectionSubTitle'] = 'We work - You rest';
$lang['workingProcessSectionStepOneTitle'] = 'Free measurement';
$lang['workingProcessSectionStepOneSubTitle'] = 'Our specialists will make all the measurements and help to choice the material.';
$lang['workingProcessSectionStepTwoTitle'] = 'Fast production';
$lang['workingProcessSectionStepTwoSubTitle'] = 'Only the best materials. In the shortest possible time for your comfort.';
$lang['workingProcessSectionStepThreeTitle'] = 'Prompt delivery';
$lang['workingProcessSectionStepThreeSubTitle'] = 'We won\'t keep you waiting long because we know the value of time.';

/**
 * Our projects section
 */
$lang['ourProjectsSectionTitle'] = 'Our Projects';
$lang['ourProjectsSectionSubTitle'] = 'Check out<br> our projects';

/**
 * Who we are section
 */
$lang['whoWeAreSectionTitle'] = 'Who are we?';
$lang['whoWeAreSectionSubTitle'] = 'Leaders in the production of metal tiles and profiled flooring.';
$lang['whoWeAreSectionDescription'] = 'The main priority of the company is to provide a full range of services, from free departure to the facility for measurements, to free delivery of manufactured products.';
$lang['whoWeAreSectionPointOneTitle'] = 'Today LIDER';
$lang['whoWeAreSectionPointOneSubTitle'] = 'One of the leading companies in the roofing materials market.';
$lang['whoWeAreSectionPointTwoTitle'] = 'Direction of Company';
$lang['whoWeAreSectionPointTwoSubTitle'] = 'The LIDER Group of companies is a reliable supplier of roofing materials and finishing elements from the world\'s leading manufacturers.';

/**
 * Counters section
 */
$lang['countersSectionPointOne'] = 'Residential Projects';
$lang['countersSectionPointTwo'] = 'Commercial Projects';
$lang['countersSectionPointThree'] = 'Satisfied Customers';

/**
 * Our certificates section
 */
$lang['ourCertificatesSectionTitle'] = 'Our Certificates';
$lang['ourCertificatesSectionSubTitle'] = 'Check out<br> our certificates';

/**
 * Contact Us section
 */
$lang['contactUsFormTitle'] = 'Need help<br> from our specialist?';
$lang['contactUsFormInputNamePlaceholder'] = 'Name';
$lang['contactUsFormInputPhonePlaceholder'] = 'Phone';
$lang['contactUsFormSubmitButtonText'] = 'Call me';

/**
 * Branches section
 */
$lang['branchesSectionTitle'] = 'Our Branches';

/**
 * Footer section
 */
$lang['footerSectionSubLogoText'] = 'Production and sale of materials for construction and finishing works.';
$lang['footerSectionSchedulerTitle'] = 'Work schedule:';
$lang['footerSectionSchedulerMoFr'] = 'Mo. - Fr. 09:00–18:00';
$lang['footerSectionSchedulerSa'] = 'Sa. 09:00–14:00';
$lang['footerSectionSchedulerSu'] = 'Su. - day off';
$lang['footerSectionContactsTitle'] = 'Adress';
$lang['footerSectionContactsCallUs'] = 'Call us:';
$lang['footerSectionContactsPhone'] = '0(22) 312 121';
$lang['footerSectionContactsEmail'] = 'info@lider.md';
$lang['footerSectionCopyrightPhraseOne'] = 'Copyright';
$lang['footerSectionCopyrightPhraseTwo'] = 'All rights reserved.';

/**
 * Order Call-back modal
 */
$lang['orderCallbackModalTitle'] = 'Request received';
$lang['orderCallbackModalText'] = 'We got the request. Soon a representative of the company will contact you.';
$lang['orderCallbackModalCloseButtonText'] = 'Close';
