<?php defined('BASEPATH') OR exit('No direct script access allowed');
$config['appSettings'] = array (
  'default_title' => 
  array (
    'ro' => 'LIDER',
    'ru' => 'LIDER',
    'en' => 'LIDER',
  ),
  'default_mk' => 
  array (
    'ro' => 'LIDER, металлочерепица, Молдова',
    'ru' => 'LIDER, металлочерепица, Молдова',
    'en' => 'LIDER, металлочерепица, Молдова',
  ),
  'default_md' => 
  array (
    'ro' => 'LIDER, металлочерепица, Молдова',
    'ru' => 'LIDER, металлочерепица, Молдова',
    'en' => 'LIDER, металлочерепица, Молдова',
  ),
  'default_address' => 
  array (
    'ro' => 'str. Petricani 31/С, Chișinău 2059, Moldova',
    'ru' => 'Молдова, Кишинев, Ул. Петрикань, 31',
    'en' => 'str. Petricani 31/С, Chisinau 2059, Moldova ',
  ),
  'default_phone' => 
  array (
    'ro' => '0(22) 312 121',
    'ru' => '0(22) 312 121',
    'en' => '0(22) 312 121',
  ),
  'default_email' => 
  array (
    'ro' => 'info@lider.md',
    'ru' => 'info@lider.md',
    'en' => 'info@lider.md',
  ),
);
