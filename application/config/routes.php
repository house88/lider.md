<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route['default_controller'] = "home";

$route['signin'] = "auth/signin";
$route['logout'] = "auth/logout";
$route['tinymanager/init/(:any)'] = "tinymanager/init/$1";

// ADMIN
$route['admin/([a-zA-Z0-9\/\-:._]+)'] = function ($module){
	$full_route = explode('/', $module);
	
	$module_name = array_shift($full_route);
	switch ($module_name) {
		case 'ajax_operations':
			return 'admin/'.$module;
		break;
		case 'trade-in':
			return 'admin/tradein';
		break;
		case 'pcode-generator':
			return 'promo_code/admin/generator';
		break;
		case 'groups':
		case 'rights':
		case 'group_rights':
			$url[] = 'users';
			$url[] = $module_name;
		break;
		case 'staff':
			$url[] = 'users/admin';
			$url[] = $module_name;
		break;
		case 'prices':
		case 'dynamic_prices':
		case 'comments':
			$url[] = 'items';
			$url[] = $module_name;
		break;
		case 'dynamic_prices':
		case 'moderate':
		case 'moderator_report':
		case 'moderator_report_new':
			$url[] = 'items/admin';
			$url[] = $module_name;
		break;
		case 'moderation_reports':
			$url[] = 'items/reports';
		break;
		default:
			$url[] = $module_name;
			$url[] = 'admin';			
		break;
	}
	
	if(!empty($full_route)){
		$url[] = implode('/', $full_route);
	}
	
	return implode('/', $url);
};

$route['^(ro|ru|en)$'] = $route['default_controller'];
$route['^(ro|ru|en)/(.+)$'] = "$2";
$route['404_override'] = '';

//$route['translate_uri_dashes'] = FALSE;
$route['scaffolding_trigger'] = "";
