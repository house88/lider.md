<?php defined('BASEPATH') OR exit('No direct script access allowed');
$config['apanel_themes'] = array (
  'default' => 
  array (
    'name' => 'Adminlte v.3',
    'views_path' => 'apanel/adminlte3/',
    'assets_path' => 'theme/apanel/adminlte3/',
  ),
);
$config['public_themes'] = array (
  'default' => 
  array (
    'id_theme' => '1',
    'name' => 'Lider v.1',
    'views_path' => 'public/v1/',
    'assets_path' => 'theme/lider/',
    'is_active' => 1,
    'is_default' => 1,
  ),
);
